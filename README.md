# README #

This application contains all the source code for the HNC NFC Application (Smart ID), created as a main project by Benjamin Leigh for the HNC Engineering (Electronic Design) course.

* Version 1

### What is this application for? ###

* Allows users to write, read erase data onto tags, as well as encrypt data. Also includes a small case study of a student enrolment system

### Where is all the code? ###

* All code is located under the Source option on the side bar. The main .class files are located under src/com/magicard/smartid/, in which the database folder contains the class files for the case study database part of the application. The layout .xml files are located under res/layout

### Who do I talk to? ###

* Any questions, e-mail: ben.leigh@ultraid.com