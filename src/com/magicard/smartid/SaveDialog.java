package com.magicard.smartid;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.magicard.smartid.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

public class SaveDialog extends DialogFragment {
	
	public static final String TAG = "SaveDialog";
	public static final String EXTRA_MESSAGE = "message";
	ArrayList<String> payloads;
	ArrayList<String> recordTypes;
	
	ArrayList<String> names = new ArrayList<String>();
	
	View layout;
	String saveName;
	String filePicked;
	
	boolean saveFailed = false;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		payloads = new ArrayList<String>();
		recordTypes = new ArrayList<String>();
		// Default for if the user doesn't want to overwrite anything
		ArrayList<String> fileNames = new ArrayList<String>();
		fileNames.add("[N/A]");
		
		Bundle b = getArguments();
		
		payloads = b.getStringArrayList("payloads");
		recordTypes = b.getStringArrayList("recordTypes");
		
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		layout = getActivity().getLayoutInflater().inflate(R.layout.activity_save_dialog, null);
		builder.setView(layout);
		// Get current files from the internal storage, for overwriting method
		SharedPreferences sharedPref = getActivity().getSharedPreferences(SaveDialog.class.toString(), Context.MODE_PRIVATE);
		Set<String> set = sharedPref.getStringSet("key", null);
		if (set !=null) {
			for (String str : set) {
				if (!str.equals("")) {
					fileNames.add(str);
				}
			}
		}
		final Spinner spnList = (Spinner) layout.findViewById(R.id.spn_save_overwrite);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, fileNames);
		spnList.setAdapter(adapter);
		spnList.setOnItemSelectedListener(new OnItemSelectedListener() {
			// User has picked a file, this one will be overwritten
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				filePicked = spnList.getItemAtPosition(position).toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
			
		});
		
		if (fileNames.size() <= 1) {
			spnList.setEnabled(false);
		}
		
		builder.setNegativeButton("Cancel", null);
		
		builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				EditText etSaveName = (EditText) layout.findViewById(R.id.et_save_name);
				saveName = etSaveName.getText().toString();
				
				if (!filePicked.equals("[N/A]")) {
					saveName = filePicked;
				}
				if (saveName.equals("")) {
					dialog.cancel();
					Toast.makeText(getActivity(), "No name supplied", Toast.LENGTH_SHORT).show();
				}
				// Get name the user picked, or the file selected, and write to it
				try {
					FileOutputStream fos = getActivity().openFileOutput(saveName, Context.MODE_PRIVATE);
					ObjectOutputStream oos = new ObjectOutputStream(fos);
					oos.writeObject(payloads);
					oos.close();
				} catch (Exception e) {
					e.printStackTrace();
					saveFailed = true;
				}
				try {
					FileOutputStream fos = getActivity().openFileOutput(saveName + "_recordTypes", Context.MODE_PRIVATE);
					ObjectOutputStream oos = new ObjectOutputStream(fos);
					oos.writeObject(recordTypes);
					oos.close();
					if (!saveFailed) {
						Toast.makeText(getActivity(), "Saved to file '" + saveName + "'!", Toast.LENGTH_SHORT).show();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				saveFailed = false;
				// Write to the internal storage the new file name
				SharedPreferences sharedPref = getActivity().getSharedPreferences(SaveDialog.class.toString(), Context.MODE_PRIVATE);
				SharedPreferences.Editor editor = sharedPref.edit();
				names.add(saveName);
				Set<String> set = new HashSet<String>();
				set.addAll(names);
				editor.putStringSet("key", set);
				editor.apply();
			}
		});
		
		return builder.create();
	}
	
	@Override
	public void onResume() {
		names.clear();
		SharedPreferences sharedPref = getActivity().getSharedPreferences(SaveDialog.class.toString(), Context.MODE_PRIVATE);
		Set<String> set = sharedPref.getStringSet("key", null);
		if (set !=null) {
			for (String str : set) {
				names.add(str);
			}
		}
		super.onResume();
	}

}
