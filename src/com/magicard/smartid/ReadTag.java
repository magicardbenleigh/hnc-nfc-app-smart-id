package com.magicard.smartid;

import java.nio.charset.Charset;
import java.security.spec.KeySpec;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.magicard.smartid.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class ReadTag extends Activity {
	
	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
	private IntentFilter[] mFilters;
	private String[][] mTechLists;
	
	boolean decryptFailed = false;
	
	NdefRecord[] records;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_read_tag);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
		mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
				getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
		mFilters = new IntentFilter[] { ndef, };
		mTechLists = new String[][] { new String[] { Ndef.class.getName() },
				new String[] { NdefFormatable.class.getName() },
				new String[] { NfcA.class.getName() } };
				
		Tag tagFromIntent = getIntent().getParcelableExtra(NfcAdapter.EXTRA_TAG);
		if(tagFromIntent != null){
			handleIntent(getIntent());
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			startActivity(new Intent(this, MainActivity.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	// Got this so the app doesn't exit if you scan the tag accidently
	@Override
	public void onResume() {
		super.onResume();
		if (mNfcAdapter != null)
			mNfcAdapter.enableForegroundDispatch(this, mPendingIntent,
					mFilters, mTechLists);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mNfcAdapter != null) {
			mNfcAdapter.disableForegroundDispatch(this);
		}
	}
	
	// Save current data for a device orientation change, or if backgrounding
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		TextView tvuid = (TextView) findViewById(R.id.tv_uid);
		savedInstanceState.putString("UID", tvuid.getText().toString());
		TextView tvtagTech = (TextView) findViewById(R.id.tv_tag_tech);
		savedInstanceState.putString("TagTech", tvtagTech.getText().toString());
		TextView tvSak = (TextView) findViewById(R.id.tv_sak);
		savedInstanceState.putString("SAK", tvSak.getText().toString());
		TextView tvAtqa = (TextView) findViewById(R.id.tv_atqa);
		savedInstanceState.putString("ATQA", tvAtqa.getText().toString());
		TextView tvTagType = (TextView) findViewById(R.id.tv_tag_type);
		savedInstanceState.putString("TagType", tvTagType.getText().toString());
		TextView tvTagSize = (TextView) findViewById(R.id.tv_tag_size);
		savedInstanceState.putString("TagSize", tvTagSize.getText().toString());
		TextView tvReadOnly = (TextView) findViewById(R.id.tv_read_only);
		savedInstanceState.putString("ReadOnly", tvReadOnly.getText().toString());
		TextView tvDataType = (TextView) findViewById(R.id.tv_data_type);
		savedInstanceState.putString("DataType", tvDataType.getText().toString());
		savedInstanceState.putParcelableArray("records", records);
	}
	
	// Restore current data after a device orientation change, or if resumed from background
	@Override
	public void onRestoreInstanceState(final Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		if (savedInstanceState.getString("UID").length() > 1) {
			RelativeLayout rlUid = (RelativeLayout) findViewById(R.id.rl_uid);
			rlUid.setVisibility(View.VISIBLE);
			rlUid.setOnLongClickListener(new View.OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View v) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							ReadTag.this);
					builder.setMessage("Do you wish to copy '" + 
							savedInstanceState.getString("UID") + "' to the clipboard?");
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Utility.copyContentToClipboard("copyUid",
											savedInstanceState.getString("UID"),
											getBaseContext());
								}
							});
					builder.setNegativeButton("No", null).show();
					return false;
				}
			});
			TextView tvuid = (TextView) findViewById(R.id.tv_uid);
			tvuid.setText(savedInstanceState.getString("UID"));
		}
		if (savedInstanceState.getString("TagTech").length() > 1) {
			RelativeLayout rlTagTech = (RelativeLayout) findViewById(R.id.rl_tag_tech);
			rlTagTech.setVisibility(View.VISIBLE);
			TextView tvtagTech = (TextView) findViewById(R.id.tv_tag_tech);
			tvtagTech.setText(savedInstanceState.getString("TagTech"));
		}
		if (savedInstanceState.getString("SAK").length() > 1) {
			RelativeLayout rlSak = (RelativeLayout) findViewById(R.id.rl_sak);
			rlSak.setVisibility(View.VISIBLE);
			TextView tvSak = (TextView) findViewById(R.id.tv_sak);
			tvSak.setText(savedInstanceState.getString("SAK"));
		}
		if (savedInstanceState.getString("ATQA").length() > 1) {
			RelativeLayout rlAtqa = (RelativeLayout) findViewById(R.id.rl_atqa);
			rlAtqa.setVisibility(View.VISIBLE);
			TextView tvAtqa = (TextView) findViewById(R.id.tv_atqa);
			tvAtqa.setText(savedInstanceState.getString("ATQA"));
		}
		if (savedInstanceState.getString("TagType").length() > 1) {
			RelativeLayout rlTagType = (RelativeLayout) findViewById(R.id.rl_tag_type);
			rlTagType.setVisibility(View.VISIBLE);
			TextView tvTagType = (TextView) findViewById(R.id.tv_tag_type);
			tvTagType.setText(savedInstanceState.getString("TagType"));
		}
		if (savedInstanceState.getString("TagSize").length() > 1) {
			RelativeLayout rlTagSize = (RelativeLayout) findViewById(R.id.rl_tag_size);
			rlTagSize.setVisibility(View.VISIBLE);
			TextView tvTagSize = (TextView) findViewById(R.id.tv_tag_size);
			tvTagSize.setText(savedInstanceState.getString("TagSize"));
		}
		if (savedInstanceState.getString("ReadOnly").length() > 1) {
			RelativeLayout rlReadOnly = (RelativeLayout) findViewById(R.id.rl_read_only);
			rlReadOnly.setVisibility(View.VISIBLE);
			TextView tvReadOnly = (TextView) findViewById(R.id.tv_read_only);
			tvReadOnly.setText(savedInstanceState.getString("ReadOnly"));
		}
		if (savedInstanceState.getString("DataType").length() > 1) {
			RelativeLayout rlDataType = (RelativeLayout) findViewById(R.id.rl_data_type);
			rlDataType.setVisibility(View.VISIBLE);
			TextView tvDataType = (TextView) findViewById(R.id.tv_data_type);
			tvDataType.setText(savedInstanceState.getString("DataType"));
		}
		if (savedInstanceState.getParcelableArray("records") != null) {
			records = (NdefRecord[]) savedInstanceState.getParcelableArray("records");
			displayNdefRecords(records);
		}
	}
	
	@Override
	public void onNewIntent(Intent intent) {
		handleIntent(intent);
	}

	private void handleIntent(Intent intent) {
		// Get Tag UID
		String tagUid = "";
		if (intent.getAction().equals(NfcAdapter.ACTION_TECH_DISCOVERED)) {
			tagUid = Utility.byteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));
			final String copyUid = tagUid;
			RelativeLayout rlUid = (RelativeLayout) findViewById(R.id.rl_uid);
			rlUid.setVisibility(0);
			rlUid.setOnLongClickListener(new View.OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View v) {
					AlertDialog.Builder builder = new AlertDialog.Builder(
							ReadTag.this);
					builder.setMessage("Do you wish to copy '" + copyUid + "' to the clipboard?");
					builder.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									Utility.copyContentToClipboard("copyUid", copyUid, getBaseContext());
								}
							});
					builder.setNegativeButton("No", null).show();
					return false;
				}
			});
			TextView tvuid = (TextView) findViewById(R.id.tv_uid);
			tvuid.setText(tagUid);
		}
		// Get Tag tech
		Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		String techListOutput = Arrays.toString(tag.getTechList());
		RelativeLayout rlTagTech = (RelativeLayout) findViewById(R.id.rl_tag_tech);
		rlTagTech.setVisibility(0);
		TextView tvtagTech = (TextView) findViewById(R.id.tv_tag_tech);
		tvtagTech.setText(techListOutput);
		// NDEF Information
		int tagSize = 0;
		Ndef ndef = Ndef.get(tag);
		try {
			ndef.connect();
			// Get tag Size
			tagSize = ndef.getMaxSize();
			RelativeLayout rlTagSize = (RelativeLayout) findViewById(R.id.rl_tag_size);
			rlTagSize.setVisibility(View.VISIBLE);
			TextView tvTagSize = (TextView) findViewById(R.id.tv_tag_size);
			tvTagSize.setText(tagSize + " bytes");
			// Can be made read-only?
			boolean canMakeReadOnly = ndef.canMakeReadOnly();
			RelativeLayout rlReadOnly = (RelativeLayout) findViewById(R.id.rl_read_only);
			rlReadOnly.setVisibility(View.VISIBLE);
			TextView tvReadOnly = (TextView) findViewById(R.id.tv_read_only);
			if (canMakeReadOnly) {
				tvReadOnly.setText("Yes");
			} else {
				tvReadOnly.setText("No");
			}
			// Get Data type
			String dataType = ndef.getType();
			RelativeLayout rlDataType = (RelativeLayout) findViewById(R.id.rl_data_type);
			rlDataType.setVisibility(View.VISIBLE);
			TextView tvDataType = (TextView) findViewById(R.id.tv_data_type);
			RelativeLayout rlTagType = (RelativeLayout) findViewById(R.id.rl_tag_type);
			rlTagType.setVisibility(View.VISIBLE);
			TextView tvTagType = (TextView) findViewById(R.id.tv_tag_type);
			if (dataType.equals("org.nfcforum.ndef.type1")) {
				tvDataType.setText("NFC Forum Type 1");
				tvTagType.setText("Innovision / Broadcom Topaz");
			} else if (dataType.equals("org.nfcforum.ndef.type2")) {
				tvDataType.setText("NFC Forum Type 2");
			} else if (dataType.equals("org.nfcforum.ndef.type3")) {
				tvDataType.setText("NFC Forum Type 3");
				tvTagType.setText("Sony FeliCa");
			} else if (dataType.equals("org.nfcforum.ndef.type4")) {
				tvDataType.setText("NFC Forum Type 4");
			} else if (dataType.equals("org.nfcforum.ndef.typemifareclassic")) {
				// Only visible to devices with a NXP NFC chip, which
				// is not many, but will include anyway
				tvDataType.setText("NFC Forum Mifare Classic");
			} else {
				tvDataType.setText("Unknown Data Type");
			}
			if (ndef != null) {
				new ReadingTask().execute(tag);
			}
			ndef.close();
		} catch (Exception e) {
			e.printStackTrace();
			RelativeLayout rlTagSize = (RelativeLayout) findViewById(R.id.rl_tag_size);
			rlTagSize.setVisibility(View.GONE);
		}
		// Get SAK & ATQA
		String sakHex = "";
		String atqaOut = "";
		String getVersion = "";
		String nTagUltralight = "";
		NfcA nfcA = NfcA.get(tag);
		try {
			nfcA.connect();
			Short sak = nfcA.getSak();
			sakHex = Integer.toHexString(sak);
			RelativeLayout rlSak = (RelativeLayout) findViewById(R.id.rl_sak);
			rlSak.setVisibility(View.VISIBLE);
			TextView tvSak = (TextView) findViewById(R.id.tv_sak);
			tvSak.setText("0x" + sakHex);
			
			byte[] atqa = nfcA.getAtqa();
			for(int i = 0; i < atqa.length / 2; i++) {
			    byte temp = atqa[i];
			    atqa[i] = atqa[atqa.length - i - 1];
			    atqa[atqa.length - i - 1] = temp;
			}
			atqaOut = Utility.byteArrayToHexString(atqa);
			RelativeLayout rlAtqa = (RelativeLayout) findViewById(R.id.rl_atqa);
			rlAtqa.setVisibility(View.VISIBLE);
			TextView tvAtqa = (TextView) findViewById(R.id.tv_atqa);
			tvAtqa.setText("0x" + atqaOut);
			
			byte[] versionInfo = nfcA.transceive(new byte[] {(byte) 0x60});
			String versionInfoResult = Utility.byteArrayToHexString(versionInfo);
			getVersion = versionInfoResult.substring(12, 14);
			nTagUltralight = versionInfoResult.substring(4, 6);
			
			nfcA.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Compare results of SAK & ATQA
		RelativeLayout rlTagType = (RelativeLayout) findViewById(R.id.rl_tag_type);
		rlTagType.setVisibility(View.VISIBLE);
		TextView tvTagType = (TextView) findViewById(R.id.tv_tag_type);
		if (atqaOut.equals("0004") && sakHex.equals("09")) {
			tvTagType.setText("NXP MIFARE Mini");
		} else if (atqaOut.equals("0004") && sakHex.equals("08")) {
			tvTagType.setText("NXP MIFARE Classic 1k");
		} else if (atqaOut.equals("0002") && sakHex.equals("18")) {
			tvTagType.setText("NXP MIFARE Classic 4k");
		} else if (atqaOut.equals("0044") && sakHex.equals("0")) {
			// Use extra data found from nfcA to determine tag type
			if (getVersion.equals("0F")) {
				tvTagType.setText("NTAG 213");
			} else if (nTagUltralight.equals("03")) {
				tvTagType.setText("NXP MIFARE Ultralight C");
			} else if (getVersion.equals("11")) {
				tvTagType.setText("NTAG 215");
			} else if (getVersion.equals("13")) {
				tvTagType.setText("NTAG 216");
			} else if (getVersion.equals("0B")) {
				if (nTagUltralight.equals("03")) {
					tvTagType.setText("NXP MIFARE Ultralight EV1");
				} else if (nTagUltralight.equals("04")) {
					tvTagType.setText("NTAG 210");
				}
			} else if (getVersion.equals("0E")) {
				if (nTagUltralight.equals("03")) {
					tvTagType.setText("NXP MIFARE Ultralight EV1");
				} else if (nTagUltralight.equals("04")) {
					tvTagType.setText("NTAG 212");
				} else {
					tvTagType.setText("NTAG 203");
				}
			} else {
				tvTagType.setText("NXP MIFARE Ultralight");
			}
		} else if (atqaOut.equals("0344") && sakHex.equals("20")) {
			tvTagType.setText("NXP MIFARE DESFire / DESFire EV1");
		} else if (atqaOut.equals("0304") && sakHex.equals("28")) {
			tvTagType.setText("IBM JCOP31");
		} else if (atqaOut.equals("0048") && sakHex.equals("20")) {
			tvTagType.setText("IBM JCOP31 v2.4.1 / 41 v2.2");
		} else if (atqaOut.equals("0004") && sakHex.equals("28")) {
			tvTagType.setText("IBM JCOP41 v2.3.1");
		} else if (atqaOut.equals("0004") && sakHex.equals("88")) {
			tvTagType.setText("Infineon MIFARE Classic 1k");
		} else if (atqaOut.equals("0002") && sakHex.equals("98")) {
			tvTagType.setText("Gemplus MPCOS");
		} else if (atqaOut.equals("0C00") && sakHex.equals("")) {
			tvTagType.setText("Innovision R&T Jewel");
		} else if (atqaOut.equals("0002") && sakHex.equals("38")) {
			tvTagType.setText("Nokia MIFARE Classic 4k - emulated (6212 Classic)");
		} else if (atqaOut.equals("0008") && sakHex.equals("38")) {
			tvTagType.setText("Nokia MIFARE Classic 4k - emulated (6131 NFC)");
		} else if (atqaOut.equals("0042") && sakHex.equals("18")) {
			tvTagType.setText("NXP MIFARE Classic EV1 4k");
		} else if (atqaOut.equals("0004") && sakHex.equals("20")) {
			tvTagType.setText("NXP MIFARE Plus S EV1 2k");
		} else if (tagUid.substring(12, 14).equals("04")) {
			if (tagUid.substring(10, 12).equals("01")) {
				String sliIdentifier = tagUid.substring(8, 10);
				int sli = Integer.parseInt(sliIdentifier, 16);
				String eightBits = Integer.toBinaryString(sli);
				if (eightBits.length() == 7) {
					if (eightBits.substring(2, 3).equals("0") && eightBits.substring(3, 4).equals("0")) {
						tvTagType.setText("NXP ICODE SLI");
					} else if (eightBits.substring(2, 3).equals("1") && eightBits.substring(3, 4).equals("0")) {
						tvTagType.setText("NXP ICODE SLIX");
					} else if (eightBits.substring(2, 3).equals("0") && eightBits.substring(3, 4).equals("1")) {
						tvTagType.setText("NXP ICODE SLIX2");
					}
				} else if (eightBits.length() == 8) {
					if (eightBits.substring(3, 4).equals("0") && eightBits.substring(4, 5).equals("0")) {
						tvTagType.setText("NXP ICODE SLI");
					} else if (eightBits.substring(3, 4).equals("1") && eightBits.substring(4, 5).equals("0")) {
						tvTagType.setText("NXP ICODE SLIX");
					} else if (eightBits.substring(3, 4).equals("0") && eightBits.substring(4, 5).equals("1")) {
						tvTagType.setText("NXP ICODE SLIX2");
					}
				}
				
			} else if (tagUid.substring(10, 12).equals("02")) {
				tvTagType.setText("NXP ICODE SLIX-S");
			} else if (tagUid.substring(10, 12).equals("03")) {
				tvTagType.setText("NXP ICODE SLIX-L");
			} else {
				rlTagType.setVisibility(View.GONE);
			}
		} else {
			rlTagType.setVisibility(View.GONE);
		}
	}
	
	// Threaded task to read the tag, threaded so doesn't block the UI from interactions
	private class ReadingTask extends AsyncTask<Tag, Void, NdefRecord[]> {
		
		WorkingDialog wd = null;
		int messageLength;
		
		@Override
		protected void onPreExecute() {
			
			wd = new WorkingDialog();
			Bundle b = new Bundle();
			b.putString(WorkingDialog.EXTRA_MESSAGE, "Reading NDEF records..");
			wd.setArguments(b);
			wd.show(getFragmentManager(), WorkingDialog.TAG);	
		}

		@Override
		protected NdefRecord[] doInBackground(Tag... params) {
			try {
				Tag tag = params[0];
				Ndef ndef = Ndef.get(tag);
				NdefMessage ndefMessage = ndef.getNdefMessage();
				messageLength = ndefMessage.getByteArrayLength();
				records = ndefMessage.getRecords();
				return records;
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		// Can't update UI thread while doing in background task,
		// so using PostExecute to update the listview
		@Override
		protected void onPostExecute(NdefRecord[] ndefRecords) {
			TextView tvRecords = (TextView) findViewById(R.id.tv_records);
			tvRecords.setText("NDEF Records (" + messageLength + " bytes):");
			displayNdefRecords(records);
			records = null;
			wd.dismiss();
		}

	}
	
	// Separate method to update the record view to the user
	private void displayNdefRecords(final NdefRecord[] ndefRecords) {
		RelativeLayout rlRecords = (RelativeLayout) findViewById(R.id.rl_records);
		rlRecords.setVisibility(View.VISIBLE);
		rlRecords.post(new Runnable() {

			@Override
			public void run() {

				TableLayout tl = (TableLayout) findViewById(R.id.tl_ndef_records);
				tl.removeAllViews();
				TableRow tr;
				TextView tv;
				TextView tvNumber;
				int i = 0;
				if (ndefRecords != null) {
					for (final NdefRecord record : ndefRecords) {
						tr = (TableRow) getLayoutInflater().inflate(R.layout.record_table_row, tl, false);
						try {
							if (record.toMimeType().equals("application/com.example.hncnfcapp")) {
								launchPasswordDialog(record);
								tr.setOnClickListener(new View.OnClickListener() {
									
									@Override
									public void onClick(View v) {
										launchPasswordDialog(record);
									}
								});
							}
						} catch (Exception e) {
							e.printStackTrace();
							tr.setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									AlertDialog.Builder builder = new AlertDialog.Builder(
											ReadTag.this);
									final String data = new String(record.getPayload(), Charset
											.forName("US-ASCII"));
									builder.setMessage("Do you wish to open '" + data + "'?");
									builder.setPositiveButton("Yes",
											new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog,
														int which) {
													Intent intent = new Intent(Intent.ACTION_VIEW, record.toUri());
													try {
														startActivity(intent);
													} catch (Exception e) {
														try {
															Intent appRecIntent = new Intent();
															appRecIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
															appRecIntent.setPackage(new String(record.getPayload(), 
																	Charset.forName("US-ASCII")));
															startActivity(appRecIntent);
														} catch (Exception appRecException) {
															appRecException.printStackTrace();
															Toast.makeText(ReadTag.this, "Record cannot be opened!", 
																	Toast.LENGTH_SHORT).show();
														}
													}
												}
											});
									builder.setNegativeButton("No", null).show();
								}
							});
						}
						
						tr.setOnLongClickListener(new View.OnLongClickListener() {
		
							@Override
							public boolean onLongClick(View v) {
								AlertDialog.Builder builder = new AlertDialog.Builder(
										ReadTag.this);
								final String data = new String(record.getPayload(), Charset
										.forName("US-ASCII"));
								builder.setMessage("Do you wish to copy '" + data + "' to the clipboard?");
								builder.setPositiveButton("Yes",
										new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog,
													int which) {
												Utility.copyContentToClipboard("content", data, getBaseContext());
											}
										});
								builder.setNegativeButton("No", null).show();
								return false;
							}
						});
						
						tv = (TextView) tr.findViewById(R.id.table_item);
						tvNumber = (TextView) tr.findViewById(R.id.record_number);
						tvNumber.setText("Record " + i + ": ");
						tv.setText(new String(record.getPayload(), Charset
								.forName("US-ASCII")));
						tl.addView(tr);	
						i++;
					}
				}
			}
		});
	}
	
	// Create the dialog to get the user to enter a password for decryption
	private void launchPasswordDialog(final NdefRecord record) {
		AlertDialog.Builder dialog = new AlertDialog.Builder(ReadTag.this);
		final View layout = getLayoutInflater().inflate(R.layout.dialog_user_password, null);
		TextView tvPassMessage = (TextView) layout.findViewById(R.id.tv_password_message);
		tvPassMessage.setVisibility(View.GONE);
		dialog.setView(layout);
		TextView tvPasswordTitle = (TextView) layout.findViewById(R.id.tv_password_title);
		tvPasswordTitle.setText("Enter password to decrypt");
		
		EditText etConfirmpassword = (EditText) layout.findViewById(R.id.et_confirm_password);
		etConfirmpassword.setVisibility(View.GONE);
		
		dialog.setPositiveButton("Decrypt", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				EditText etPassword = (EditText) layout.findViewById(R.id.et_password);
				char[] password = etPassword.getText().toString().toCharArray();
				decryptData(record, password);
			}
			
		});
		dialog.setNegativeButton("Cancel", null);
		dialog.show();
	}
	
	// Actual decrypting of the data for the user
	private void decryptData(final NdefRecord record, final char[] password) {
		new Thread() {
			public void run() {
				WorkingDialog wd = new WorkingDialog();
				Bundle b = new Bundle();
				b.putString(WorkingDialog.EXTRA_MESSAGE, "Decrypting data..");
				wd.setArguments(b);
				wd.show(getFragmentManager(), WorkingDialog.TAG);	
				byte[] resultArray = record.getPayload();
				String resultString = Utility.byteArrayToHexString(resultArray);
				String dataString = resultString.substring(2, Math.max(0, resultString.length() - 64));
				String IVString = resultString.substring(Math.max(0, resultString.length() - 64), Math.max(0, resultString.length() - 32));
				String saltString = resultString.substring(Math.max(0, resultString.length() - 32));
				
				byte[] encryptedData = Utility.hexStringToByteArray(dataString);
				byte[] salt = Utility.hexStringToByteArray(saltString);
				byte[] iv = Utility.hexStringToByteArray(IVString);
				
				try {
					KeySpec keySpec = new PBEKeySpec(password, salt, Other.iterations, Other.lengthOfKey);
					SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
					byte[] keyInBytes = keyFactory.generateSecret(keySpec).getEncoded();
					SecretKey key = new SecretKeySpec(keyInBytes, "AES");
					
					Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
					IvParameterSpec ivParams = new IvParameterSpec(iv);
					cipher.init(Cipher.DECRYPT_MODE, key, ivParams);
					byte[] decryptedData = cipher.doFinal(encryptedData);
					NdefMessage decryptedMessage = new NdefMessage(decryptedData);
					NdefRecord[] newRecords = decryptedMessage.getRecords();
					records = newRecords;
					wd.dismiss();
					displayNdefRecords(newRecords);
				} catch (Exception e) {
					e.printStackTrace();
					wd.dismiss();
				    runOnUiThread(new Runnable() {
				        public void run() { 
				            Toast.makeText(ReadTag.this, "Incorrect Password", Toast.LENGTH_SHORT).show();
				        } 
				    }); 
				}
			}
		}.start();
	}
	
}
