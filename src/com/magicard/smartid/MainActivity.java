package com.magicard.smartid;

import com.magicard.smartid.R;
import com.magicard.smartid.database.DatabaseActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTitle("");
        setContentView(R.layout.activity_main);
		TextView tv = (TextView) findViewById(R.id.activity_main_store_link);
	    tv.setText(Html.fromHtml("<a href=https://play.google.com/store/apps/details?id=com.magicard.trustid> Visit App store here"));
	    tv.setMovementMethod(LinkMovementMethod.getInstance());
    }
    
    public void WriteToTag(View v) {
		Intent intent = new Intent(this, WriteToTag.class);
		startActivity(intent);
    }
    
    public void ReadTag(View v) {
    	Intent intent = new Intent(this, ReadTag.class);
    	startActivity(intent);
    }
    
    public void EraseTag(View v) {
    	Intent intent = new Intent(this, EraseTag.class);
    	startActivity(intent);
    }
    
    public void Other(View v) {
    	Intent intent = new Intent(this, Other.class);
    	startActivity(intent);
    }
    
    public void DatabaseActivity(View v) {
    	Intent intent = new Intent(this, DatabaseActivity.class);
    	startActivity(intent);
    }
    
}
