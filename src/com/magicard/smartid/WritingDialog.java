package com.magicard.smartid;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import com.magicard.smartid.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.nfc.NdefRecord;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class WritingDialog extends DialogFragment implements OnCheckedChangeListener {
	
	WritingDialogListener callback;
	
	public static final String SET_RECORD = "setRecord";
	public static final String ADD_PLAIN_TEXT = "addPlainText";
	public static final String ADD_WEB_ADDRESS = "addWebAddress";
	public static final String ADD_PHONE_NUMBER = "addPhoneNumber";
	public static final String ADD_EMAIL = "addEmail";
	public static final String ADD_SMS = "addSMS";
	public static final String ADD_GEO = "addGeo";
	public static final String ADD_CUSTOM_MESSAGE = "addCusMes";
	public static final String ADD_APP_RECORD = "addAppRecord";
	public static final String ADD_BLUETOOTH = "addBluetooth";
	public static final String ADD_CONTACT = "addContact";
	public static final String ADD_SOCIAL = "addSocial";
	public static final String EDIT_PLAIN_TEXT = "editPlainText";
	public static final String EDIT_WEB_ADDRESS = "editWebAddress";
	public static final String EDIT_PHONE_NUMBER = "editPhoneNumber";
	public static final String EDIT_EMAIL = "editEmail";
	public static final String EDIT_SMS = "editSMS";
	public static final String EDIT_GEO = "editGeo";
	public static final String EDIT_CUSTOM_MESSAGE = "editCusMes";
	public static final String EDIT_APP_RECORD = "editAppRecord";
	public static final String EDIT_BLUETOOTH = "editBluetooth";
	public static final String EDIT_CONTACT = "editContact";
	public static final String EDIT_SOCIAL = "editSocial";
	
	public static final String TAG = "WritingDialog";
	
	String coords;
	String socialChoice;
	EditText etlat;
	EditText etlon;
	EditText etAppRec;
	ArrayList<String> titleList;
	ArrayList<String> subList;
	ArrayList<Drawable> icon;
	boolean replace = false;
	boolean edit = false;
	View nfclayout;
	
	private static final String PATTERN = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$";
	
	int flag = 0;
	
	// Returns to WriteToTag page for each record type
	public interface WritingDialogListener {
		public void onAddText(NdefRecord nr1, boolean edit);
		public void onAddWebAddress(NdefRecord nr1, boolean edit);
		public void onAddPhoneNumber(NdefRecord nr1, boolean edit);
		public void onAddEmail(NdefRecord nr1, String emailReturn, boolean edit);
		public void onAddSms(NdefRecord nr1, String smsReturn, boolean edit);
		public void onAddGeo(NdefRecord nr1, String geoReturn, boolean edit);
		public void onAddCustomMessage(NdefRecord nr1, String cusReturn, boolean edit);
		public void onAddAppRecord(NdefRecord nr1, boolean edit);
		public void onAddBluetooth(NdefRecord nr1, String macReturn, boolean edit);
		public void onAddContact(NdefRecord nr1, String contactReturn, boolean edit);
		public void onAddSocial(NdefRecord nr1, boolean edit);
	}
	
	// Set up to ensure WriteToTag is linked to this
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback = (WritingDialogListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement WritingDialogListener");
		}
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		nfclayout = getActivity().getLayoutInflater().inflate(
				R.layout.activity_writing_dialog, null);
		builder.setView(nfclayout);
		
		Bundle b = getArguments();
		// Check if add text has been pressed
		if (b.getString(SET_RECORD).equals(ADD_PLAIN_TEXT)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_plain_text);
			layout.setVisibility(View.VISIBLE);
			flag = 1;
		} // Edit / Replace text
		if (b.getString(SET_RECORD).equals(EDIT_PLAIN_TEXT)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_plain_text);
			layout.setVisibility(View.VISIBLE);
			if (b.getString("plainText") != null) {
				EditText etPlainText = (EditText) layout.findViewById(R.id.et_plain_text);
				etPlainText.setText(b.getString("plainText"));
				edit = true;
			} else {
				replace = true;
			}
			flag = 1;
		} // Add URL
		if (b.getString(SET_RECORD).equals(ADD_WEB_ADDRESS)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_web_address);
			layout.setVisibility(View.VISIBLE);
			flag = 2;
		} // Edit / Replace URL
		if (b.getString(SET_RECORD).equals(EDIT_WEB_ADDRESS)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_web_address);
			layout.setVisibility(View.VISIBLE);
			EditText etUrl = (EditText) layout.findViewById(R.id.et_web_address);
			if (b.getString("URL") != null) {
				etUrl.setText(b.getString("URL"));
				edit = true;
			} else {
				replace = true;
			}
			flag = 2;
		} // Add Phone Number
		if (b.getString(SET_RECORD).equals(ADD_PHONE_NUMBER)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_phone_number);
			layout.setVisibility(View.VISIBLE);
			flag = 3;
		} // Edit / Replace Phone Number
		if (b.getString(SET_RECORD).equals(EDIT_PHONE_NUMBER)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_phone_number);
			layout.setVisibility(View.VISIBLE);
			EditText etPhone = (EditText) layout.findViewById(R.id.et_phone_number);
			if (b.getString("phoneNumber") != null) {
				etPhone.setText(b.getString("phoneNumber"));
				edit = true;
			} else {
				replace = true;
			}
			flag = 3;
		} // Add E-mail
		if (b.getString(SET_RECORD).equals(ADD_EMAIL)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_email);
			layout.setVisibility(View.VISIBLE);
			flag = 4;
		} // Edit / Replace E-mail
		if (b.getString(SET_RECORD).equals(EDIT_EMAIL)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_email);
			layout.setVisibility(View.VISIBLE);
			if (b.getString("emailAddress") != null || b.getString("emailSubject") != null || b.getString("emailMessage") != null) {
				EditText etemailAddress = (EditText) layout.findViewById(R.id.et_email_address);
				etemailAddress.setText(b.getString("emailAddress"));
				EditText etemailSubject = (EditText) layout.findViewById(R.id.et_email_subject);
				etemailSubject.setText(b.getString("emailSubject"));
				EditText etemailMessage = (EditText) layout.findViewById(R.id.et_email_message);
				etemailMessage.setText(b.getString("emailMessage"));
				edit = true;
			} else {
				replace = true;
			}
			flag = 4;
		} // Add text message
		if (b.getString(SET_RECORD).equals(ADD_SMS)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_sms);
			layout.setVisibility(View.VISIBLE);
			flag = 5;
		} // Edit / Replace text message
		if (b.getString(SET_RECORD).equals(EDIT_SMS)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_sms);
			layout.setVisibility(View.VISIBLE);
			if (b.getString("smsNumber") != null || b.getString("smsMessage") != null) {
				EditText etSmsNumber = (EditText) layout.findViewById(R.id.et_sms_number);
				etSmsNumber.setText(b.getString("smsNumber"));
				EditText etSmsMessage = (EditText) layout.findViewById(R.id.et_sms_message);
				etSmsMessage.setText(b.getString("smsMessage"));
				edit = true;
			} else {
				replace = true;
			}
			flag = 5;
		} // Add Geo-location
		if (b.getString(SET_RECORD).equals(ADD_GEO)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_geo);
			layout.setVisibility(View.VISIBLE);
			CheckBox cb = (CheckBox) nfclayout.findViewById(R.id.cb_geo_cur_loc);
			cb.setOnCheckedChangeListener(this);
			flag = 6;
		} // Edit / Replace Geo-location
		if (b.getString(SET_RECORD).equals(EDIT_GEO)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_geo);
			layout.setVisibility(View.VISIBLE);
			CheckBox cb = (CheckBox) nfclayout.findViewById(R.id.cb_geo_cur_loc);
			cb.setOnCheckedChangeListener(this);
			if (b.getString("geoLat") !=null || b.getString("geoLon") != null) {
				EditText etLat = (EditText) nfclayout.findViewById(R.id.et_geo_lat);
				etLat.setText(b.getString("geoLat"));
				EditText etLon = (EditText) nfclayout.findViewById(R.id.et_geo_lon);
				etLon.setText(b.getString("geoLon"));
				edit = true;
			} else {
				replace = true;
			}
			flag = 6;
		} // Add custom message
		if (b.getString(SET_RECORD).equals(ADD_CUSTOM_MESSAGE)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_cus_mes);
			layout.setVisibility(View.VISIBLE);
			flag = 7;
		} // Edit / Replace custom message
		if (b.getString(SET_RECORD).equals(EDIT_CUSTOM_MESSAGE)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_cus_mes);
			layout.setVisibility(View.VISIBLE);
			if (b.getString("cusMime1") != null || b.getString("cusMime2") !=null || b.getString("cusMess") != null) {
				EditText etMime1 = (EditText) layout.findViewById(R.id.et_cusmes_mime_a);
				etMime1.setText(b.getString("cusMime1"));
				EditText etMime2 = (EditText) layout.findViewById(R.id.et_cusmes_mime_b);
				etMime2.setText(b.getString("cusMime2"));
				EditText etMess = (EditText) layout.findViewById(R.id.et_cusmes_message);
				etMess.setText(b.getString("cusMess"));
				edit = true;
			} else {
				replace = true;
			}
			flag = 7;
		} // Add / Edit / Replace application record
		if (b.getString(SET_RECORD).equals(ADD_APP_RECORD) || b.getString(SET_RECORD).equals(EDIT_APP_RECORD)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_app_record);
			layout.setVisibility(View.VISIBLE);
			PackageManager pm = getActivity().getPackageManager();
			Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
			mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);
			titleList = new ArrayList<String>();
			subList = new ArrayList<String>();
			icon = new ArrayList<Drawable>();
			ArrayList<ResolveInfo> pkgAppsList = (ArrayList<ResolveInfo>) pm.queryIntentActivities(
					mainIntent, PackageManager.PERMISSION_GRANTED);
			for (ResolveInfo info : pkgAppsList) {
				titleList.add(info.activityInfo.applicationInfo.loadLabel(pm)
						.toString());
				subList.add(info.activityInfo.applicationInfo.packageName
						.toString());
				icon.add(info.activityInfo.applicationInfo.loadIcon(pm));
			}
			Spinner spinner = (Spinner) layout.findViewById(R.id.spn_apprec);
			etAppRec = (EditText) layout.findViewById(R.id.et_apprec_package_name);
			spinner.setAdapter(new AppRecordAdapter(getActivity().getBaseContext(),
					R.layout.app_record_spinner_item, titleList));
			spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> where,
						View clickedid, int position, long row) {
					String result = subList.get(position);
					etAppRec.setText(result);
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					etAppRec.setHint("com.magicard.trustid");
				}
			});
			if (b.getString("appRecord") != null) {
				int selection = subList.indexOf(b.getString("appRecord"));
				spinner.setSelection(selection);
				edit = true;
			} 
			if (b.getString("appRecord") == null && b.getString(SET_RECORD).equals(EDIT_APP_RECORD)) {
				replace = true;
			}
			flag = 8;
		} // Add MAC Address
		if (b.getString(SET_RECORD).equals(ADD_BLUETOOTH)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_bluetooth);
			layout.setVisibility(View.VISIBLE);
			flag = 9;
		} // Edit / Replace MAC Address
		if (b.getString(SET_RECORD).equals(EDIT_BLUETOOTH)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_bluetooth);
			layout.setVisibility(View.VISIBLE);
			if (b.getString("blueRecord") != null) {
				EditText etMacAddress = (EditText) layout.findViewById(R.id.et_mac_address);
				etMacAddress.setText(b.getString("blueRecord"));
				edit = true;
			} else {
				replace = true;
			}
			flag = 9;
		} // Add vCard contact
		if (b.getString(SET_RECORD).equals(ADD_CONTACT)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_contact);
			layout.setVisibility(View.VISIBLE);
			flag = 10;
		} // Edit / Replace vCard contact
		if (b.getString(SET_RECORD).equals(EDIT_CONTACT)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_contact);
			layout.setVisibility(View.VISIBLE);
			if (b.getString("contactFN") != null || b.getString("contactOrg") != null || b.getString("contactPhone") != null ||
					b.getString("contactEmail") != null || b.getString("contactUrl") != null) {
				EditText etContFN = (EditText) layout.findViewById(R.id.et_contact_fn);
				etContFN.setText(b.getString("contactFN"));
				EditText etContOrg = (EditText) layout.findViewById(R.id.et_contact_org);
				etContOrg.setText(b.getString("contactOrg"));
				EditText etContPhone = (EditText) layout.findViewById(R.id.et_contact_phone);
				etContPhone.setText(b.getString("contactPhone"));
				EditText etContEmail = (EditText) layout.findViewById(R.id.et_contact_email);
				etContEmail.setText(b.getString("contactEmail"));
				EditText etContURL = (EditText) layout.findViewById(R.id.et_contact_url);
				etContURL.setText(b.getString("contactUrl"));
				edit = true;
			} else {
				replace = true;
			}
			flag = 10;
		} // Add / Edit / Replace social media link
		if (b.getString(SET_RECORD).equals(ADD_SOCIAL) || b.getString(SET_RECORD).equals(EDIT_SOCIAL)) {
			RelativeLayout layout = (RelativeLayout) nfclayout.findViewById(R.id.rl_social_media);
			layout.setVisibility(View.VISIBLE);
			final ArrayList<String> medias = new ArrayList<String>();
			medias.add("Facebook");
			medias.add("Twitter");
			medias.add("Instagram");
			medias.add("LinkedIn");
			medias.add("Google+");
			Spinner spinner = (Spinner) layout.findViewById(R.id.spn_social);
			spinner.setAdapter(new ArrayAdapter<String>(getActivity().getBaseContext(),
					android.R.layout.simple_spinner_dropdown_item, medias));
			spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					socialChoice = medias.get(position);
				}
				@Override
				public void onNothingSelected(AdapterView<?> parent) {
				}
			});
			if (b.getString("socialTotal") != null) {
				String socialTotal = b.getString("socialTotal");
				String userName = "";
				if (socialTotal.startsWith("insta")) {
					userName = socialTotal.substring(14);
					int insta = medias.indexOf("Instagram");
					spinner.setSelection(insta);
				} else if (socialTotal.startsWith("www.face")) {
					userName = socialTotal.substring(17);
					int face = medias.indexOf("Facebook");
					spinner.setSelection(face);
				} else if (socialTotal.startsWith("www.twitt")) {
					userName = socialTotal.substring(16);
					int twit = medias.indexOf("Twitter");
					spinner.setSelection(twit);
				} else if (socialTotal.startsWith("www.linked")) {
					userName = socialTotal.substring(20);
					int link = medias.indexOf("LinkedIn");
					spinner.setSelection(link);
				} else if(socialTotal.startsWith("plus.goo")) {
					userName = socialTotal.substring(17);
					int plus = medias.indexOf("Google+");
					spinner.setSelection(plus);
				}
				EditText etUser = (EditText) layout.findViewById(R.id.et_social_username);
				etUser.setText(userName);
				edit = true;
			} 
			if (b.getString(SET_RECORD).equals(EDIT_SOCIAL) && b.getString("socialTotal") == null) {
				replace = true;
			}
			flag = 11;
		}
		String buttonText;
		if (edit) {
			buttonText = "Edit";
		} else if (replace) {
			edit = true;
			replace = false;
			buttonText = "Replace";
		} else {
			buttonText = "Add";
		}
		
		builder.setPositiveButton(buttonText, new DialogInterface.OnClickListener() {
			// When user presses OK, work out which option is selected and create a record from that
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (flag == 1) {
					flag = 0;
					EditText etpt = (EditText) nfclayout.findViewById(R.id.et_plain_text);
					String plainTextString = etpt.getText().toString();
					byte[] uriField = plainTextString.getBytes(Charset.forName("US-ASCII"));
					byte[] payload = new byte[uriField.length + 1];
					System.arraycopy(uriField, 0, payload, 1, uriField.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
							NdefRecord.RTD_URI, new byte[0], payload);
					callback.onAddText(nr1, edit);
				}
				if (flag == 2) {
					flag = 0;
					EditText etwa = (EditText) nfclayout.findViewById(R.id.et_web_address);
					String plainTextString = etwa.getText().toString();
					byte[] uriField = plainTextString.getBytes(Charset.forName("US-ASCII"));
					byte[] payload = new byte[uriField.length + 1];
					payload[0] = 0x03;
					System.arraycopy(uriField, 0, payload, 1, uriField.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
							NdefRecord.RTD_URI, new byte[0], payload);
					callback.onAddWebAddress(nr1, edit);

				}
				if (flag == 3) {
					flag = 0;
					EditText etpn = (EditText) nfclayout.findViewById(R.id.et_phone_number);
					String plainTextString = etpn.getText().toString();
					byte[] uriField = plainTextString.getBytes(Charset.forName("US-ASCII"));
					byte[] payload = new byte[uriField.length + 1];
					payload[0] = 0x05;
					System.arraycopy(uriField, 0, payload, 1, uriField.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
							NdefRecord.RTD_URI, new byte[0], payload);
					callback.onAddPhoneNumber(nr1, edit);
				}
				if (flag == 4) {
					flag = 0;
					EditText etemailAddress = (EditText) nfclayout.findViewById(R.id.et_email_address);
					String emailAddress = etemailAddress.getText().toString();
					EditText etemailSubject = (EditText) nfclayout.findViewById(R.id.et_email_subject);
					String emailSubject = etemailSubject.getText().toString();
					EditText etemailMessage = (EditText) nfclayout.findViewById(R.id.et_email_message);
					String emailMessage = etemailMessage.getText().toString();
					String email = emailAddress.concat("?subject=").concat(emailSubject).concat("&body=").concat(emailMessage);
					String emailReturn = "Address: ".concat(emailAddress).concat("\nSubject: ").concat(emailSubject).concat("\nMessage: ").concat(emailMessage);
					byte[] uriField = email.getBytes(Charset.forName("US-ASCII"));
					byte[] payload = new byte[uriField.length + 1];
					payload[0] = 0x06;
					System.arraycopy(uriField, 0, payload, 1, uriField.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
							NdefRecord.RTD_URI, new byte[0], payload);
					callback.onAddEmail(nr1, emailReturn, edit);
				}
				if (flag == 5) {
					flag = 0;
					EditText etSmsNumber = (EditText) nfclayout.findViewById(R.id.et_sms_number);
					String smsNumber = etSmsNumber.getText().toString();
					EditText etSmsMessage = (EditText) nfclayout.findViewById(R.id.et_sms_message);
					String smsMessage = etSmsMessage.getText().toString();
					String sms = "sms:".concat(smsNumber).concat("?body=").concat(smsMessage);
					String smsReturn = "Number: ".concat(smsNumber).concat("\nMessage: ").concat(smsMessage);
					byte[] uriField = sms.getBytes(Charset.forName("US-ASCII"));
					byte[] payload = new byte[uriField.length + 1];
					System.arraycopy(uriField, 0, payload, 1, uriField.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
							NdefRecord.RTD_URI, new byte[0], payload);
					callback.onAddSms(nr1, smsReturn, edit);
				}
				if (flag == 6) {
					flag = 0;
					EditText etlat = (EditText) nfclayout.findViewById(R.id.et_geo_lat);
					String latitude = etlat.getText().toString();
					EditText etlon = (EditText) nfclayout.findViewById(R.id.et_geo_lon);
					String longitude = etlon.getText().toString();
					coords = "geo:".concat(latitude).concat(",").concat(longitude);
					String geoReturn = "Latitude: ".concat(latitude).concat("\nLongitude: ").concat(longitude);
					byte[] uriField = coords.getBytes(Charset.forName("US-ASCII"));
					byte[] payload = new byte[uriField.length + 1];
					System.arraycopy(uriField, 0, payload, 1, uriField.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
							NdefRecord.RTD_URI, new byte[0], payload);
					callback.onAddGeo(nr1, geoReturn, edit);
				}
				if (flag == 7) {
					flag = 0;
					EditText etMimeA = (EditText) nfclayout.findViewById(R.id.et_cusmes_mime_a);
					EditText etMimeB = (EditText) nfclayout.findViewById(R.id.et_cusmes_mime_b);
					String mime = etMimeA.getText().toString().concat("/").concat(etMimeB.getText().toString());
					EditText etMimeMessage = (EditText) nfclayout.findViewById(R.id.et_cusmes_message);
					String mimeMessage = etMimeMessage.getText().toString();
					String cusReturn = "Mime: ".concat(mime + "\n").concat("Message: " + mimeMessage);
					byte[] uriField = mimeMessage.getBytes(Charset.forName("US-ASCII"));
					byte[] payload = new byte[uriField.length + 1];
					System.arraycopy(uriField, 0, payload, 1, uriField.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
							mime.getBytes(), new byte[0], payload);
					callback.onAddCustomMessage(nr1, cusReturn, edit);
				}
				if (flag == 8) {
					flag = 0;
					NdefRecord nr1;
					try {
						String appRecord = etAppRec.getText().toString();
						nr1 = NdefRecord.createApplicationRecord(appRecord);
					} catch (Exception e) {
						nr1 = NdefRecord.createApplicationRecord("com.magicard.trustid");
					}
					callback.onAddAppRecord(nr1, edit);
				}
				if (flag == 9) {
					flag = 0;
					EditText etMacAdd = (EditText) nfclayout.findViewById(R.id.et_mac_address);
					String macAddress = etMacAdd.getText().toString();
					String bluetoothMimeType = "application/vnd.bluetooth.ep.oob";
					if (macAddress.matches(PATTERN)) {
						String filteredMacAdd = macAddress.replace(":", "");
						filteredMacAdd = filteredMacAdd.replace("-", "");
						filteredMacAdd  = Integer.toString(18).concat(filteredMacAdd);
						byte[] uriField = Utility.hexStringToByteArray(filteredMacAdd);
						byte[] payload = new byte[uriField.length + 1];
						System.arraycopy(uriField, 0, payload, 1,
								uriField.length);
						NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_MIME_MEDIA, bluetoothMimeType
								.getBytes(), new byte[0], payload);
						callback.onAddBluetooth(nr1, filteredMacAdd, edit);
					} else {
						Toast.makeText(getActivity(), "Invalid MAC Address",
								Toast.LENGTH_SHORT).show();
					}
				}
				if (flag == 10) {
					flag = 0;
					EditText etContactFN = (EditText) nfclayout.findViewById(R.id.et_contact_fn);
					String contactFN = etContactFN.getText().toString();
					EditText etContactOrg = (EditText) nfclayout.findViewById(R.id.et_contact_org);
					String contactOrg = etContactOrg.getText().toString();
					EditText etContactPhone = (EditText) nfclayout.findViewById(R.id.et_contact_phone);
					String contactPhone = etContactPhone.getText().toString();
					EditText etContactEmail = (EditText) nfclayout.findViewById(R.id.et_contact_email);
					String contactEmail = etContactEmail.getText().toString();
					EditText etContactURL = (EditText) nfclayout.findViewById(R.id.et_contact_url);
					String contactURL = etContactURL.getText().toString();
					String forRecord = "BEGIN:VCARD\nVERSION:3.0\n".concat("FN:" + contactFN + "\n").concat("ORG:" + contactOrg + "\n")
							.concat("TEL:" + contactPhone + "\n").concat("EMAIL:" + contactEmail + "\n").concat("URL:" + contactURL + "\nEND:VCARD");
					String forReturn = "Full Name: ".concat(contactFN + "\nCompany: ").concat(contactOrg + "\nPhone Number: ")
							.concat(contactPhone + "\nE-mail Address: ").concat(contactEmail + "\nWeb Page: ").concat(contactURL + "\n");
					byte[] uriField = forRecord.getBytes(Charset
							.forName("US-ASCII"));
					byte[] payload = new byte[uriField.length + 1];
					System.arraycopy(uriField, 0, payload, 1, uriField.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
							"text/vcard".getBytes(), new byte[0], payload);
					callback.onAddContact(nr1, forReturn, edit);
				}
				if (flag == 11) {
					flag = 0;
					String urlAddress = "";
					if (socialChoice.equals("Facebook")) {
						urlAddress = "www.facebook.com/";
					}else if (socialChoice.equals("Twitter")) {
						urlAddress = "www.twitter.com/";
					}else if (socialChoice.equals("Instagram")) {
						urlAddress = "instagram.com/";
					}else if (socialChoice.equals("LinkedIn")) {
						urlAddress = "www.linkedin.com/in/";
					}else if (socialChoice.equals("Google+")) {
						urlAddress = "plus.google.com/+";
					}
					EditText etUsername = (EditText) nfclayout.findViewById(R.id.et_social_username);
					String username = etUsername.getText().toString();
					urlAddress = urlAddress.concat(username);
					byte[] uriField = urlAddress.getBytes(Charset.forName("US-ASCII"));
					byte[] payload = new byte[uriField.length + 1];
					payload[0] = 0x03;
					System.arraycopy(uriField, 0, payload, 1, uriField.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
							NdefRecord.RTD_URI, new byte[0], payload);
					callback.onAddSocial(nr1, edit);
				}
			}
		});
		
		builder.setNegativeButton("Cancel", null);
		
		return builder.create();
	}
	
	// Listener for if get current location is pressed
	@Override
	public void onCheckedChanged(CompoundButton buttonView,
			boolean isChecked) {
		etlat = (EditText) nfclayout.findViewById(R.id.et_geo_lat);
		etlon = (EditText) nfclayout.findViewById(R.id.et_geo_lon);
		if (isChecked) {
			etlat.setEnabled(false);
			etlon.setEnabled(false);
			etlat.setText("Waiting for GPS data...");
			etlon.setText("Waiting for GPS data...");
			LocationManager locationManager = (LocationManager) getActivity()
					.getSystemService(Context.LOCATION_SERVICE);
			LocationListener locationListener = new RecordLocationListener();
			if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				locationManager.requestLocationUpdates(
						LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
			} else if (locationManager
					.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
				locationManager.requestLocationUpdates(
						LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
			} else {
				CheckBox cb = (CheckBox) nfclayout.findViewById(R.id.cb_geo_cur_loc);
				cb.setChecked(false);
				etlat.setEnabled(true);
				etlon.setEnabled(true);
				etlat.setText("");
				etlon.setText("");
				Toast.makeText(getActivity(), "GPS is disabled", Toast.LENGTH_SHORT).show();
			}
		} else {
			etlat.setEnabled(true);
			etlon.setEnabled(true);
		}
	}
	
	// Listener that find the current location - using GPS availability of users device
	public class RecordLocationListener implements LocationListener {

		@Override
		public void onLocationChanged(Location location) {
			location.getLatitude();
			location.getLongitude();

			coords = "geo:" + location.getLatitude() + ","
					+ location.getLongitude();
			
			etlat.setText(String.valueOf(location.getLatitude()));
			etlon.setText(String.valueOf(location.getLongitude()));
		}

		@Override
		public void onProviderDisabled(String arg0) {
		}

		@Override
		public void onProviderEnabled(String arg0) {
		}

		@Override
		public void onStatusChanged(String arg0, int status, Bundle extras) {
		}
	}
	
	// View adapter - for application record spinner
	public class AppRecordAdapter extends ArrayAdapter<String> {

		public AppRecordAdapter(Context mContext, int textViewResourceId,
				List<String> myList) {
			super(mContext, textViewResourceId, myList);
		}

		public View getDropDownView(int position, View convertView,
				ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}

		private View getCustomView(int position, View convertView,
				ViewGroup parent) {
			LayoutInflater inflater = getActivity().getLayoutInflater();
			View row = inflater.inflate(R.layout.app_record_spinner_item,
					parent, false);
			TextView title = (TextView) row.findViewById(R.id.appName);
			title.setText(titleList.get(position).toString());
			TextView sub = (TextView) row.findViewById(R.id.packageName);
			sub.setText(subList.get(position).toString());
			ImageView icons = (ImageView) row.findViewById(R.id.image);
			icons.setBackground(icon.get(position));
			return row;
		}
	}
}
