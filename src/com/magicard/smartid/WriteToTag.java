package com.magicard.smartid;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Collections;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.magicard.smartid.R;
import com.magicard.smartid.LoadDialog.LoadDialogListener;
import com.magicard.smartid.WritingDialog.WritingDialogListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.DialogInterface.OnClickListener;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;
import android.widget.Toast;

public class WriteToTag extends Activity implements WritingDialogListener, OnMenuItemClickListener, LoadDialogListener {
	
	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
	private IntentFilter[] mFilters;
	private String[][] mTechLists;
	
	NdefRecord nr1;
	ArrayList<NdefRecord> ndefRecords;
	ArrayList<String> payloads;
	ArrayList<String> recordTypes;
	NdefMessage ndefMessage;
	ListView lvPayloadOutput;
	int listPosition;
	
	int UP = 1695;
	int DOWN = 5195;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_write_to_tag);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
		mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
				getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
		mFilters = new IntentFilter[] { ndef, };
		mTechLists = new String[][] { new String[] { Ndef.class.getName() },
				new String[] { NdefFormatable.class.getName() },
				new String[] { NfcA.class.getName() } };
		
		ndefRecords = new ArrayList<NdefRecord>();
		payloads = new ArrayList<String>();
		recordTypes = new ArrayList<String>();
		
		lvPayloadOutput = (ListView) findViewById(R.id.listView1);
		
		Button goButton = (Button) findViewById(R.id.write_go_button);
		goButton.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					ndefMessage = new NdefMessage(ndefRecords.toArray(new NdefRecord[ndefRecords.size()]));
					TextView tv1 = (TextView) findViewById(R.id.textViewUserMessage);
					CheckBox cbEncrypt = (CheckBox) findViewById(R.id.write_cb_encrypt);
					if (cbEncrypt.isChecked()) {
						if (userPassword !=null) {
							encryptData();
						} else {
							cbEncrypt.setChecked(false);
							Toast.makeText(getBaseContext(), "No password set!", Toast.LENGTH_SHORT).show();
						}
					} else {
						tv1.setText("Scan tag to write data (" + ndefMessage.toByteArray().length + " Bytes)");
					}
				} catch (Exception e) {
					e.printStackTrace();
					Toast.makeText(getBaseContext(), "Error creating NdefMessage....aborting", Toast.LENGTH_SHORT).show();
				}
				
			}
		});
		
		CheckBox cbEncrypt = (CheckBox) findViewById(R.id.write_cb_encrypt);
		cbEncrypt.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if (isChecked) {
					setPassword();
				} else {
					userPassword = "";
				}
			}
		});
		
		lvPayloadOutput.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, final View view,
					final int position, long id) {
				listPosition = position;
				final PopupMenu popup = new PopupMenu(WriteToTag.this, view);
				popup.getMenuInflater().inflate(R.menu.list_view_options,
						popup.getMenu());
				
				if (listPosition >= 1 && listPosition <= payloads.size()) {
					popup.getMenu().add(Menu.NONE, UP, Menu.NONE, "Move Up");
				}
				if (listPosition < payloads.size() - 1) {
					popup.getMenu().add(Menu.NONE, DOWN, Menu.NONE, "Move Down");
				}
				
				popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

					@Override
					public boolean onMenuItemClick(MenuItem item) {
						int itemID = item.getItemId();
						
						if (itemID == UP) {
							Collections.swap(ndefRecords, listPosition, listPosition - 1);
							Collections.swap(payloads, listPosition, listPosition - 1);
							Collections.swap(recordTypes, listPosition, listPosition - 1);
							lvPayloadOutput.setAdapter(new RecordAdapter(WriteToTag.this,
									android.R.layout.activity_list_item, payloads));
						} else if (itemID == DOWN) {
							Collections.swap(ndefRecords, listPosition, listPosition + 1);
							Collections.swap(payloads, listPosition, listPosition + 1);
							Collections.swap(recordTypes, listPosition, listPosition + 1);
							lvPayloadOutput.setAdapter(new RecordAdapter(WriteToTag.this,
									android.R.layout.activity_list_item, payloads));
						} else if (itemID == R.id.action_delete_item) {
							View viewToAnimate = lvPayloadOutput.getChildAt(position);
							Animation anim = AnimationUtils.makeOutAnimation(getBaseContext(), true);
							anim.setAnimationListener(new AnimationListener() {

								@Override
								public void onAnimationStart(Animation animation) {}

								@Override
								public void onAnimationEnd(Animation animation) {
									lvPayloadOutput.post(new Runnable() {

										@Override
										public void run() {
											ndefMessage = null;
											TextView tv1 = (TextView) findViewById(R.id.textViewUserMessage);
											tv1.setText("");
											ndefRecords.remove(position);
											payloads.remove(position);
											recordTypes.remove(position);
											lvPayloadOutput.setAdapter(new RecordAdapter(WriteToTag.this,
													android.R.layout.activity_list_item, payloads));
										}
									});	
								}

								@Override
								public void onAnimationRepeat(
										Animation animation) {}
							});
							if (viewToAnimate != null) {
								viewToAnimate.startAnimation(anim);
							}

						} else if (itemID == R.id.action_copy_item) {
							String payload = payloads.get(position);
							Utility.copyContentToClipboard("copyRecord", payload, WriteToTag.this);
						} else if (itemID == R.id.action_edit) {
							TextView tv1 = (TextView) findViewById(R.id.textViewUserMessage);
							tv1.setText("");
							ndefMessage = null;
							if (recordTypes.get(position).startsWith("Plain Text")) {
								String plainText = payloads.get(position);
								Bundle b = new Bundle();
								WritingDialog writingDialog = new WritingDialog();
								b.putString(WritingDialog.SET_RECORD, WritingDialog.EDIT_PLAIN_TEXT);
								b.putString("plainText", plainText.trim());
								writingDialog.setArguments(b);
								writingDialog.show(getFragmentManager(), WritingDialog.TAG);
							}
							if (recordTypes.get(position).startsWith("Web Address")) {
								String URL = payloads.get(position);
								Bundle b = new Bundle();
								WritingDialog writingDialog = new WritingDialog();
								b.putString(WritingDialog.SET_RECORD, WritingDialog.EDIT_WEB_ADDRESS);
								b.putString("URL", URL.substring(1));
								writingDialog.setArguments(b);
								writingDialog.show(getFragmentManager(), WritingDialog.TAG);
							}
							if (recordTypes.get(position).startsWith("Phone Number")) {
								String phoneNumber = payloads.get(position);
								Bundle b = new Bundle();
								WritingDialog writingDialog = new WritingDialog();
								b.putString(WritingDialog.SET_RECORD, WritingDialog.EDIT_PHONE_NUMBER);
								b.putString("phoneNumber", phoneNumber.substring(1).trim());
								writingDialog.setArguments(b);
								writingDialog.show(getFragmentManager(), WritingDialog.TAG);
							}
							if (recordTypes.get(position).startsWith("E-mail")) {
								String emailTotal = payloads.get(position);
								int email1 = emailTotal.indexOf("Address: ");
								int email2 = emailTotal.indexOf("Subject: ");
								int email3 = emailTotal.indexOf("Message: ");
								String emailAddress = emailTotal.substring(email1 + 9, email2).trim();
								String emailSubject = emailTotal.substring(email2 + 9, email3).trim();
								String emailMessage = emailTotal.substring(email3 + 9).trim();
								Bundle b = new Bundle();
								WritingDialog writingDialog = new WritingDialog();
								b.putString(WritingDialog.SET_RECORD, WritingDialog.EDIT_EMAIL);
								b.putString("emailAddress", emailAddress);
								b.putString("emailSubject", emailSubject);
								b.putString("emailMessage", emailMessage);
								writingDialog.setArguments(b);
								writingDialog.show(getFragmentManager(), WritingDialog.TAG);
							}
							if (recordTypes.get(position).startsWith("SMS")) {
								String smsTotal = payloads.get(position);
								int sms1 = smsTotal.indexOf("Number: ");
								int sms2 = smsTotal.indexOf("Message: ");
								String smsNumber = smsTotal.substring(sms1 + 8, sms2).trim();
								String smsMessage = smsTotal.substring(sms2 + 9).trim();
								Bundle b = new Bundle();
								WritingDialog writingDialog = new WritingDialog();
								b.putString(WritingDialog.SET_RECORD, WritingDialog.EDIT_SMS);
								b.putString("smsNumber", smsNumber);
								b.putString("smsMessage", smsMessage);
								writingDialog.setArguments(b);
								writingDialog.show(getFragmentManager(), WritingDialog.TAG);
							}
							if (recordTypes.get(position).startsWith("Geo Location")) {
								String geoTotal = payloads.get(position);
								int geo1 = geoTotal.indexOf("Latitude: ");
								int geo2 = geoTotal.indexOf("Longitude: ");
								String geoLat = geoTotal.substring(geo1 + 10, geo2).trim();
								String geoLon = geoTotal.substring(geo2 + 11).trim();
								Bundle b = new Bundle();
								WritingDialog writingDialog = new WritingDialog();
								b.putString(WritingDialog.SET_RECORD, WritingDialog.EDIT_GEO);
								b.putString("geoLat", geoLat);
								b.putString("geoLon", geoLon);
								writingDialog.setArguments(b);
								writingDialog.show(getFragmentManager(), WritingDialog.TAG);
							}
							if (recordTypes.get(position).startsWith("Custom Message")) {
								String cusTotal = payloads.get(position);
								int cus1 = cusTotal.indexOf("Mime: ");
								int cus2 = cusTotal.indexOf("/");
								int cus3 = cusTotal.indexOf("Message: ");
								String cusMime1 = cusTotal.substring(cus1 + 6, cus2).trim();
								String cusMime2 = cusTotal.substring(cus2 + 1, cus3).trim();
								String cusMess = cusTotal.substring(cus3 + 9).trim();
								Bundle b = new Bundle();
								WritingDialog writingDialog = new WritingDialog();
								b.putString(WritingDialog.SET_RECORD, WritingDialog.EDIT_CUSTOM_MESSAGE);
								b.putString("cusMime1", cusMime1);
								b.putString("cusMime2", cusMime2);
								b.putString("cusMess", cusMess);
								writingDialog.setArguments(b);
								writingDialog.show(getFragmentManager(), WritingDialog.TAG);
							}
							if (recordTypes.get(position).startsWith("Application Record")) {
								String appRecord = payloads.get(position);
								Bundle b = new Bundle();
								WritingDialog writingDialog = new WritingDialog();
								b.putString(WritingDialog.SET_RECORD, WritingDialog.EDIT_APP_RECORD);
								b.putString("appRecord", appRecord);
								writingDialog.setArguments(b);
								writingDialog.show(getFragmentManager(), WritingDialog.TAG);
							}
							if (recordTypes.get(position).startsWith("MAC Address")) {
								String blueRecord = payloads.get(position);
								Bundle b = new Bundle();
								WritingDialog writingDialog = new WritingDialog();
								b.putString(WritingDialog.SET_RECORD, WritingDialog.EDIT_BLUETOOTH);
								b.putString("blueRecord", blueRecord);
								writingDialog.setArguments(b);
								writingDialog.show(getFragmentManager(), WritingDialog.TAG);
							}
							if (recordTypes.get(position).startsWith("Contact")) {
								String contactTotal = payloads.get(position);
								int contact1 = contactTotal.indexOf("Full Name: ");
								int contact2 = contactTotal.indexOf("Company: ");
								int contact3 = contactTotal.indexOf("Phone Number: ");
								int contact4 = contactTotal.indexOf("E-mail Address: ");
								int contact5 = contactTotal.indexOf("Web Page: ");
								String contactFN = contactTotal.substring(contact1 + 11, contact2).trim();
								String contactOrg = contactTotal.substring(contact2 + 9, contact3).trim();
								String contactPhone = contactTotal.substring(contact3 + 14, contact4).trim();
								String contactEmail = contactTotal.substring(contact4 + 16, contact5).trim();
								String contactUrl = contactTotal.substring(contact5 + 10).trim();
								Bundle b = new Bundle();
								WritingDialog writingDialog = new WritingDialog();
								b.putString(WritingDialog.SET_RECORD, WritingDialog.EDIT_CONTACT);
								b.putString("contactFN", contactFN);
								b.putString("contactOrg", contactOrg);
								b.putString("contactPhone", contactPhone);
								b.putString("contactEmail", contactEmail);
								b.putString("contactUrl", contactUrl);
								writingDialog.setArguments(b);
								writingDialog.show(getFragmentManager(), WritingDialog.TAG);
							}
							if (recordTypes.get(position).startsWith("Social Media")) {
								String socialTotal = payloads.get(position);
								Bundle b = new Bundle();
								WritingDialog writingDialog = new WritingDialog();
								b.putString(WritingDialog.SET_RECORD, WritingDialog.EDIT_SOCIAL);
								b.putString("socialTotal", socialTotal.substring(1));
								writingDialog.setArguments(b);
								writingDialog.show(getFragmentManager(), WritingDialog.TAG);
							}
						} else if (itemID == R.id.action_replace) {
							TextView tv1 = (TextView) findViewById(R.id.textViewUserMessage);
							tv1.setText("");
							ndefMessage = null;
							PopupMenu popupMenu = new PopupMenu(WriteToTag.this, view);
							popupMenu.getMenuInflater().inflate(R.menu.add_option_list, popupMenu.getMenu());
							popupMenu.setOnMenuItemClickListener(new OnMenuItemClickListener() {

								@Override
								public boolean onMenuItemClick(MenuItem item) {
									if (item.getItemId() == R.id.add_plain_text) {
										startWritingDialog(WritingDialog.EDIT_PLAIN_TEXT);
									}
									else if (item.getItemId() == R.id.add_web_address) {
										startWritingDialog(WritingDialog.EDIT_WEB_ADDRESS);
									}
									else if (item.getItemId() == R.id.add_phone_number) {
										startWritingDialog(WritingDialog.EDIT_PHONE_NUMBER);
									}
									else if (item.getItemId() == R.id.add_email) {
										startWritingDialog(WritingDialog.EDIT_EMAIL);
									}
									else if (item.getItemId() == R.id.add_sms) {
										startWritingDialog(WritingDialog.EDIT_SMS);
									}
									else if (item.getItemId() == R.id.add_geo) {
										startWritingDialog(WritingDialog.EDIT_GEO);
									}
									else if (item.getItemId() == R.id.add_cus_mes) {
										startWritingDialog(WritingDialog.EDIT_CUSTOM_MESSAGE);
									}
									else if (item.getItemId() == R.id.add_app_record) {
										startWritingDialog(WritingDialog.EDIT_APP_RECORD);
									}
									else if (item.getItemId() == R.id.add_bluetooth) {
										startWritingDialog(WritingDialog.EDIT_BLUETOOTH);
									}
									else if (item.getItemId() == R.id.add_contact) {
										startWritingDialog(WritingDialog.EDIT_CONTACT);
									}
									else if (item.getItemId() == R.id.add_social_media) {
										startWritingDialog(WritingDialog.EDIT_SOCIAL);
									}
									return false;
								}
								
							});
							popupMenu.show();
							return true;
						}
						return false;
					}
					
				});
				popup.show();
			}
			
		});
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.add_new_record, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			startActivity(new Intent(this, MainActivity.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			return true;
		case R.id.action_add:
			View topCornerView = findViewById(R.id.action_add);
			PopupMenu popupMenu = new PopupMenu(this, topCornerView);
			popupMenu.getMenuInflater().inflate(R.menu.add_option_list, popupMenu.getMenu());
			popupMenu.setOnMenuItemClickListener(this);
			popupMenu.show();
			return true;
		case R.id.action_delete_all:
			if (payloads.size() > 0) {
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage("Are you sure you wish to delete all records; this operation CANNOT be reversed!");
				builder.setPositiveButton("Delete",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Animation anim = AnimationUtils.makeOutAnimation(WriteToTag.this, true);
								anim.setAnimationListener(new AnimationListener() {

									@Override
									public void onAnimationStart(Animation animation) {}

									@Override
									public void onAnimationEnd(Animation animation) {
										lvPayloadOutput.post(new Runnable() {

											@Override
											public void run() {
												ndefRecords.clear();
												payloads.clear();
												recordTypes.clear();
												TextView tv1 = (TextView) findViewById(R.id.textViewUserMessage);
												tv1.setText("");
												lvPayloadOutput.setAdapter(new RecordAdapter(WriteToTag.this,
														android.R.layout.activity_list_item, payloads));
											}
										});	
									}

									@Override
									public void onAnimationRepeat(
											Animation animation) {}
								});
								lvPayloadOutput.startAnimation(anim);
							}
						});
				builder.setNegativeButton("Cancel", null).show();
			}
			return true;
		case R.id.action_load:
			LoadDialog loadDialog = new LoadDialog();
			loadDialog.show(getFragmentManager(), LoadDialog.TAG);
			return true;
		case R.id.action_save:
			if (payloads.size() > 0) {
				Bundle b = new Bundle();
				b.putStringArrayList("payloads", payloads);
				b.putStringArrayList("recordTypes", recordTypes);
				SaveDialog saveDialog = new SaveDialog();
				saveDialog.setArguments(b);
				saveDialog.show(getFragmentManager(), SaveDialog.TAG);
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putStringArrayList("payloads", payloads);
		outState.putStringArrayList("recordTypes", recordTypes);
		outState.putParcelableArrayList("ndefRecords", ndefRecords);
		outState.putInt("listPosition", listPosition);
	}

	@Override
	protected void onRestoreInstanceState(Bundle outState) {
		super.onRestoreInstanceState(outState);
		payloads = outState.getStringArrayList("payloads");
		recordTypes = outState.getStringArrayList("recordTypes");
		ndefRecords = outState.getParcelableArrayList("ndefRecords");
		listPosition = outState.getInt("listPosition");
		lvPayloadOutput.setAdapter(new RecordAdapter(WriteToTag.this,
				android.R.layout.activity_list_item, payloads));
	}
	
	// Got this so the app doesn't exit if you scan the tag accidently
	@Override
	public void onResume() {
		super.onResume();
		if (mNfcAdapter != null)
			mNfcAdapter.enableForegroundDispatch(this, mPendingIntent,
					mFilters, mTechLists);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mNfcAdapter != null) {
			mNfcAdapter.disableForegroundDispatch(this);
		}
	}
	
	// Called when a tag is scanned
	@Override
	public void onNewIntent(Intent intent) {
		handleIntent(intent);
	}

	//Takes intent from tag scanning, and starts the writing
	private void handleIntent(Intent intent) {
		Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		new WritingTask().execute(tag);
	}

	// Return from WritingDialog for adding / editing text
	@Override
	public void onAddText(NdefRecord nr1, boolean edit) {
		if (edit) {
			ndefRecords.set(listPosition, nr1);
			payloads.set(listPosition, new String(nr1.getPayload(), Charset.forName("US-ASCII")));
			recordTypes.set(listPosition, "Plain Text: ");
		} else {
			ndefRecords.add(nr1);
			payloads.add(new String(nr1.getPayload(), Charset.forName("US-ASCII")));
			recordTypes.add("Plain Text: ");
		}
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
	}
	
	// Return from WritingDialog for adding / editing url's
	@Override
	public void onAddWebAddress(NdefRecord nr1, boolean edit) {
		if (edit) {
			ndefRecords.set(listPosition, nr1);
			payloads.set(listPosition, new String(nr1.getPayload(), Charset.forName("US-ASCII")));
			recordTypes.set(listPosition, "Web Address: ");
		} else {
			ndefRecords.add(nr1);
			payloads.add(new String(nr1.getPayload(), Charset.forName("US-ASCII")));
			recordTypes.add("Web Address: ");
		}
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
	}
	
	// Return from WritingDialog for adding / editing phone numbers
	@Override
	public void onAddPhoneNumber(NdefRecord nr1, boolean edit) {
		if (edit) {
			ndefRecords.set(listPosition, nr1);
			payloads.set(listPosition, new String(nr1.getPayload(), Charset.forName("US-ASCII")));
			recordTypes.set(listPosition, "Phone Number: ");
		} else {
			ndefRecords.add(nr1);
			payloads.add(new String(nr1.getPayload(), Charset.forName("US-ASCII")));
			recordTypes.add("Phone Number: ");
		}
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
	}
	
	// Return from WritingDialog for adding / editing emails
	@Override
	public void onAddEmail(NdefRecord nr1, String emailReturn, boolean edit) {
		if (edit) {
			ndefRecords.set(listPosition, nr1);
			payloads.set(listPosition, emailReturn);
			recordTypes.set(listPosition, "E-mail: ");
		} else {
			ndefRecords.add(nr1);
			payloads.add(emailReturn);
			recordTypes.add("E-mail: ");
		}
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
	}
	
	// Return from WritingDialog for adding / editing text messages
	@Override
	public void onAddSms(NdefRecord nr1, String smsReturn, boolean edit) {
		if (edit) {
			ndefRecords.set(listPosition, nr1);
			payloads.set(listPosition, smsReturn);
			recordTypes.set(listPosition, "SMS: ");
		} else {
			ndefRecords.add(nr1);
			payloads.add(smsReturn);
			recordTypes.add("SMS: ");
		}
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
	}
	
	// Return from WritingDialog for adding / editing geolocations
	@Override
	public void onAddGeo(NdefRecord nr1, String geoReturn, boolean edit) {
		if (edit) {
			ndefRecords.set(listPosition, nr1);
			payloads.set(listPosition, geoReturn);
			recordTypes.set(listPosition, "Geo Location: ");
		} else {
			ndefRecords.add(nr1);
			payloads.add(geoReturn);
			recordTypes.add("Geo Location: ");
		}
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
	}
	
	// Return from WritingDialog for adding / editing custom messages
	@Override
	public void onAddCustomMessage(NdefRecord nr1, String cusReturn, boolean edit) {
		if (edit) {
			ndefRecords.set(listPosition, nr1);
			payloads.set(listPosition, cusReturn);
			recordTypes.set(listPosition, "Custom Message: ");
		} else {
			ndefRecords.add(nr1);
			payloads.add(cusReturn);
			recordTypes.add("Custom Message: ");
		}
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
	}
	
	// Return from WritingDialog for adding / editing app records
	@Override
	public void onAddAppRecord(NdefRecord nr1, boolean edit) {
		if (edit) {
			ndefRecords.set(listPosition, nr1);
			payloads.set(listPosition, new String(nr1.getPayload(), Charset.forName("US-ASCII")));
			recordTypes.set(listPosition, "Application Record: ");
		} else {
			ndefRecords.add(nr1);
			payloads.add(new String(nr1.getPayload(), Charset.forName("US-ASCII")));
			recordTypes.add("Application Record: ");
		}
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
	}
	
	// Return from WritingDialog for adding / editing mac addresses
	@Override
	public void onAddBluetooth(NdefRecord nr1, String macReturn, boolean edit) {
		if (edit ) {
			ndefRecords.set(listPosition, nr1);
			payloads.set(listPosition, macReturn);
			recordTypes.set(listPosition, "MAC Address: ");
		} else {
			ndefRecords.add(nr1);
			payloads.add(macReturn);
			recordTypes.add("MAC Address: ");
		}
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
	}
	
	// Return from WritingDialog for adding / editing vCard contacts
	@Override
	public void onAddContact(NdefRecord nr1, String contactReturn, boolean edit) {
		if (edit) {
			ndefRecords.set(listPosition, nr1);
			payloads.set(listPosition, contactReturn);
			recordTypes.set(listPosition, "Contact: ");
		} else {
			ndefRecords.add(nr1);
			payloads.add(contactReturn);
			recordTypes.add("Contact: ");
		}
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
	}
	
	// Return from WritingDialog for adding / editing social medias
	@Override
	public void onAddSocial(NdefRecord nr1, boolean edit) {
		if (edit) {
			ndefRecords.set(listPosition, nr1);
			payloads.set(listPosition, new String(nr1.getPayload(), Charset.forName("US-ASCII")));
			recordTypes.set(listPosition, "Social Media: ");
		} else {
			ndefRecords.add(nr1);
			payloads.add(new String(nr1.getPayload(), Charset.forName("US-ASCII")));
			recordTypes.add("Social Media: ");
		}
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
	}
	
	@Override
	public boolean onMenuItemClick(MenuItem item) {
		if (item.getItemId() == R.id.add_plain_text) {
			startWritingDialog(WritingDialog.ADD_PLAIN_TEXT);
		}
		else if (item.getItemId() == R.id.add_web_address) {
			startWritingDialog(WritingDialog.ADD_WEB_ADDRESS);
		}
		else if (item.getItemId() == R.id.add_phone_number) {
			startWritingDialog(WritingDialog.ADD_PHONE_NUMBER);
		}
		else if (item.getItemId() == R.id.add_email) {
			startWritingDialog(WritingDialog.ADD_EMAIL);
		}
		else if (item.getItemId() == R.id.add_sms) {
			startWritingDialog(WritingDialog.ADD_SMS);
		}
		else if (item.getItemId() == R.id.add_geo) {
			startWritingDialog(WritingDialog.ADD_GEO);
		}
		else if (item.getItemId() == R.id.add_cus_mes) {
			startWritingDialog(WritingDialog.ADD_CUSTOM_MESSAGE);
		}
		else if (item.getItemId() == R.id.add_app_record) {
			startWritingDialog(WritingDialog.ADD_APP_RECORD);
		}
		else if (item.getItemId() == R.id.add_bluetooth) {
			startWritingDialog(WritingDialog.ADD_BLUETOOTH);
		}
		else if (item.getItemId() == R.id.add_contact) {
			startWritingDialog(WritingDialog.ADD_CONTACT);
		}
		else if (item.getItemId() == R.id.add_social_media) {
			startWritingDialog(WritingDialog.ADD_SOCIAL);
		}
		return false;
	}
	
	public void startWritingDialog(String string) {
		Bundle b = new Bundle();
		WritingDialog writingDialog = new WritingDialog();
		b.putString(WritingDialog.SET_RECORD, string);
		writingDialog.setArguments(b);
		writingDialog.show(getFragmentManager(), WritingDialog.TAG);
	}
	
	private class WritingTask extends AsyncTask<Tag, Void, String> {
		
		WorkingDialog wd = null;
		
		@Override
		protected void onPreExecute() {
			
			wd = new WorkingDialog();
			Bundle b = new Bundle();
			b.putString(WorkingDialog.EXTRA_MESSAGE, "Write tag..");
			wd.setArguments(b);
			wd.show(getFragmentManager(), WorkingDialog.TAG);	
		}
		
		@Override
		protected String doInBackground(Tag... params) {
			Tag tag = params[0];
			try {
				int dataSize = ndefMessage.toByteArray().length;
				Ndef ndefInstance = Ndef.get(tag);
				if (ndefInstance !=null) {
					ndefInstance.connect();
					if (ndefInstance.getMaxSize() < dataSize) {
						return "The data is too large for the tag!";
					}
					ndefInstance.writeNdefMessage(ndefMessage);
					ndefInstance.close();
					return "The data has been written to the tag!";
				} else {
					NdefFormatable ndefFormatableInstance = NdefFormatable.get(tag);
					if (ndefFormatableInstance != null) {
						try {
							ndefFormatableInstance.connect();
							ndefFormatableInstance.format(ndefMessage);
							ndefFormatableInstance.close();
							return "The data has been written to the tag!";
						} catch (IOException e) {
							return "The tag has not been formatted";
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				return "Writing to tag has failed! Have you pressed Write?";
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String string) {
			Toast.makeText(WriteToTag.this, string, Toast.LENGTH_SHORT).show();
			wd.dismiss();
		}
		
	}
	
	// Nice little custom adapter
	public class RecordAdapter extends ArrayAdapter<String> {

		public RecordAdapter(Context mContext, int textViewResourceId,
				ArrayList<String> myList) {
			super(mContext, textViewResourceId, myList);
		}

		public View getView(int position, View convertView, ViewGroup parent) {
			return getCustomView(position, convertView, parent);
		}

		private View getCustomView(int position, View convertView,
				ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View field = inflater.inflate(R.layout.add_payload_listview, parent,
					false);
			TextView tvRecordType = (TextView) field.findViewById(R.id.tv_record_type);
			tvRecordType.setText(recordTypes.get(position).toString());
			TextView tvPayload = (TextView) field.findViewById(R.id.tv_payload);
			tvPayload.setText(payloads.get(position).toString());
			return field;
		}
	}
	// Return from load dialog
	@Override
	public void onLoadRecords(ArrayList<String> payloadsReturn,
			ArrayList<String> recordTypesReturn, ArrayList<NdefRecord> ndefRecordsReturn) {
		TextView tv1 = (TextView) findViewById(R.id.textViewUserMessage);
		tv1.setText("");
		ndefMessage = null;
		payloads.clear();
		recordTypes.clear();
		ndefRecords.clear();
		payloads = payloadsReturn;
		recordTypes = recordTypesReturn;
		ndefRecords = ndefRecordsReturn;
		lvPayloadOutput.setAdapter(new RecordAdapter(this,
				android.R.layout.activity_list_item, payloads));
		Animation anim = AnimationUtils.makeInChildBottomAnimation(this);
		lvPayloadOutput.startAnimation(anim);
		
	}
	
	String userPassword;
	boolean passwordProvided;
	// Start password dialog, and get the entered data
	public Dialog setPassword() {
		final CheckBox cbEncrypt = (CheckBox) findViewById(R.id.write_cb_encrypt);
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		final View layout = getLayoutInflater().inflate(R.layout.dialog_user_password, null);
		dialog.setView(layout);
		dialog.setCancelable(false);
		dialog.setPositiveButton("Confirm", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				EditText etPassword = (EditText) layout.findViewById(R.id.et_password);
				userPassword = etPassword.getText().toString();
				if (userPassword.matches(".*\\d.*") && 
						userPassword.matches(".*[A-Z].*") && userPassword.matches(".*[a-z].*")) {
					EditText etConfirmPassword = (EditText) layout.findViewById(R.id.et_confirm_password);
					String confirmPassword = etConfirmPassword.getText().toString();
					if (confirmPassword.equals(userPassword)) {
						passwordProvided = true;
					} else {
						Toast.makeText(WriteToTag.this, "Both passwords must match", Toast.LENGTH_SHORT).show();
						cbEncrypt.setChecked(false);
					}
				} else {
					Toast.makeText(WriteToTag.this, "Password must contain at least one number, "
							+ "and contain both upper and lower case letters", Toast.LENGTH_SHORT).show();
					cbEncrypt.setChecked(false);
				}
			}
			
		});
		dialog.setNegativeButton("Cancel",new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				userPassword = ""; 
				cbEncrypt.setChecked(false);
			}
		});
		return dialog.show();
		
	}
	
	//Encryption method
	private void encryptData() {
		new Thread() {
			public void run() {
				final WorkingDialog wd = new WorkingDialog();
				Bundle b = new Bundle();
				b.putString(WorkingDialog.EXTRA_MESSAGE, "Creating encrypted data..");
				wd.setArguments(b);
				wd.show(getFragmentManager(), WorkingDialog.TAG);
				int lengthOfSalt = Other.lengthOfKey / 8;
				
				SecureRandom secRandom = new SecureRandom();
				byte[] salt = new byte[lengthOfSalt];
				secRandom.nextBytes(salt);
				KeySpec keySpec = new PBEKeySpec(userPassword.toCharArray(), salt, Other.iterations, Other.lengthOfKey);
				userPassword = "";
				try {
					SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
					byte[] keyInBytes = keyFactory.generateSecret(keySpec).getEncoded();
					SecretKey key = new SecretKeySpec(keyInBytes, "AES");
					
					Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
					byte[] iv = new byte[cipher.getBlockSize()];
					secRandom.nextBytes(iv);
					IvParameterSpec ivParams = new IvParameterSpec(iv);
					cipher.init(Cipher.ENCRYPT_MODE, key, ivParams);
					byte[] encryptedData = cipher.doFinal(ndefMessage.toByteArray());
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					outputStream.write(encryptedData);
					outputStream.write(iv);
					outputStream.write(salt);
					byte[] overallEncryption = outputStream.toByteArray();
					byte[] payload = new byte[overallEncryption.length + 1];
					System.arraycopy(overallEncryption, 0, payload, 1, overallEncryption.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
							"application/com.example.hncnfcapp".getBytes(), new byte[0], payload);
					ndefMessage = new NdefMessage(nr1);
				    runOnUiThread(new Runnable() {
				        public void run() 
				        { 
							TextView tv1 = (TextView) findViewById(R.id.textViewUserMessage);
							tv1.setText("Scan tag to write data (" + ndefMessage.toByteArray().length + " Bytes)");
				        } 
				    });
					wd.dismiss();
					
				} catch (Exception e) {
				    runOnUiThread(new Runnable() {
				        public void run() 
				        { 
				            Toast.makeText(WriteToTag.this, "Sorry, there was an error during encryption", Toast.LENGTH_SHORT).show();
				            wd.dismiss();
				        } 
				    });
					e.printStackTrace();
				}
			}
		}.start();
	}
}
