package com.magicard.smartid;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import com.magicard.smartid.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.nfc.NdefRecord;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

public class LoadDialog extends DialogFragment {

	public static final String TAG = "LoadDialog";
	public static final String EXTRA_MESSAGE = "message";
	LoadDialogListener callback;

	private static final String PATTERN = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$";

	ArrayList<String> payloads;
	ArrayList<String> recordTypes;
	ArrayList<NdefRecord> ndefRecords;

	String filePicked;
	int filePosition;

	// Listener for WriteToTag method
	public interface LoadDialogListener {
		public void onLoadRecords(ArrayList<String> payloads,
				ArrayList<String> recordTypes, ArrayList<NdefRecord> ndefRecords);
	}

	// Link back to other methods that implement this
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback = (LoadDialogListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement LoadDialogListener");
		}
	}

	// Create the load dialog
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		payloads = new ArrayList<String>();
		recordTypes = new ArrayList<String>();
		ndefRecords = new ArrayList<NdefRecord>();

		final ArrayList<String> fileNames = new ArrayList<String>();

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		View layout = getActivity().getLayoutInflater().inflate(
				R.layout.activity_load_dialog, null);
		builder.setView(layout);

		// Get files of records from internal storage
		SharedPreferences sharedPref = getActivity().getSharedPreferences(
				SaveDialog.class.toString(), Context.MODE_PRIVATE);
		Set<String> set = sharedPref.getStringSet("key", null);
		if (set != null) {
			for (String str : set) {
				if (!str.equals("")) {
					fileNames.add(str);
				}
			}
		}
		if (fileNames.size() == 0) {
			Toast.makeText(getActivity(), "No files to load!",
					Toast.LENGTH_SHORT).show();
			dismiss();
		}
		final Spinner spnList = (Spinner) layout
				.findViewById(R.id.spn_file_names);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
				android.R.layout.simple_spinner_dropdown_item, fileNames);
		spnList.setAdapter(adapter);
		spnList.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				filePosition = position;
				filePicked = spnList.getItemAtPosition(position).toString();
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}

		});
		// Delete record file from the internal storage
		Button delButton = (Button) layout.findViewById(R.id.btn_load_delete);
		delButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setMessage("Are you sure you wish to delete the file '" + filePicked + "'?\n\nThis operation CANNOT be reversed!");
				builder.setPositiveButton("Delete",
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
	
								fileNames.remove(spnList.getItemAtPosition(filePosition));
	
								Set<String> set = new HashSet<String>();
								set.addAll(fileNames);
	
								SharedPreferences sharedPref = getActivity()
										.getSharedPreferences(SaveDialog.class.toString(),
												Context.MODE_PRIVATE);
								SharedPreferences.Editor editor = sharedPref.edit();
								editor.putStringSet("key", set);
								editor.apply();
	
								getActivity().deleteFile(filePicked);
								getActivity().deleteFile(filePicked + "_recordTypes");
	
								ArrayAdapter<String> adapter = new ArrayAdapter<String>(
										getActivity(),
										android.R.layout.simple_spinner_dropdown_item,
										fileNames);
								spnList.setAdapter(adapter);
	
								if (fileNames.isEmpty()) {
									Toast.makeText(getActivity(), "No files to load!",
											Toast.LENGTH_SHORT).show();
									dismiss();
								}
							}
						});
							
				builder.setNegativeButton("Cancel", null).show();
			}

		});

		builder.setNegativeButton("Cancel", null);

		builder.setPositiveButton("Load",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Get the records from the file, and input it into the associated arrays
						try {
							FileInputStream fis = getActivity().openFileInput(
									filePicked);
							ObjectInputStream ois = new ObjectInputStream(fis);
							payloads = (ArrayList<String>) ois.readObject();
							ois.close();
						} catch (Exception e) {
							e.printStackTrace();
						}
						try {
							FileInputStream fis = getActivity().openFileInput(
									filePicked + "_recordTypes");
							ObjectInputStream ois = new ObjectInputStream(fis);
							recordTypes = (ArrayList<String>) ois.readObject();
							ois.close();
						} catch (Exception e) {
							e.printStackTrace();
						}

						createNdefRecordArray(payloads, recordTypes);

						callback.onLoadRecords(payloads, recordTypes,
								ndefRecords);

					}

				});

		return builder.create();
	}

	// Create the new record arrays (NDEF Records & user friendly list), using data from the files
	public void createNdefRecordArray(ArrayList<String> payloads,
			ArrayList<String> recordTypes) {
		for (int i = 0; i < payloads.size(); i++) {
			String typeAtLoc = recordTypes.get(i);
			if (typeAtLoc.startsWith("Plain Text:")) {
				String ptData = payloads.get(i).substring(1).trim();
				byte[] uriField = ptData.getBytes(Charset.forName("US-ASCII"));
				byte[] payload = new byte[uriField.length + 1];
				System.arraycopy(uriField, 0, payload, 1, uriField.length);
				NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
						NdefRecord.RTD_URI, new byte[0], payload);
				ndefRecords.add(nr1);
			} else if (typeAtLoc.startsWith("Web Address:")) {
				String urlData = payloads.get(i).substring(1).trim();
				byte[] uriField = urlData.getBytes(Charset.forName("US-ASCII"));
				byte[] payload = new byte[uriField.length + 1];
				payload[0] = 0x03;
				System.arraycopy(uriField, 0, payload, 1, uriField.length);
				NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
						NdefRecord.RTD_URI, new byte[0], payload);
				ndefRecords.add(nr1);
			} else if (typeAtLoc.startsWith("Phone Number:")) {
				String pnData = payloads.get(i).substring(1).trim();
				byte[] uriField = pnData.getBytes(Charset.forName("US-ASCII"));
				byte[] payload = new byte[uriField.length + 1];
				payload[0] = 0x05;
				System.arraycopy(uriField, 0, payload, 1, uriField.length);
				NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
						NdefRecord.RTD_URI, new byte[0], payload);
				ndefRecords.add(nr1);
			} else if (typeAtLoc.startsWith("E-mail:")) {
				String emailTotal = payloads.get(i);
				int email1 = emailTotal.indexOf("Address: ");
				int email2 = emailTotal.indexOf("Subject: ");
				int email3 = emailTotal.indexOf("Message: ");
				String emailAddress = emailTotal.substring(email1 + 9, email2)
						.trim();
				String emailSubject = emailTotal.substring(email2 + 9, email3)
						.trim();
				String emailMessage = emailTotal.substring(email3 + 9).trim();
				String email = emailAddress.concat("?subject=")
						.concat(emailSubject).concat("&body=")
						.concat(emailMessage);
				byte[] uriField = email.getBytes(Charset.forName("US-ASCII"));
				byte[] payload = new byte[uriField.length + 1];
				payload[0] = 0x06;
				System.arraycopy(uriField, 0, payload, 1, uriField.length);
				NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
						NdefRecord.RTD_URI, new byte[0], payload);
				ndefRecords.add(nr1);
			} else if (typeAtLoc.startsWith("SMS:")) {
				String smsTotal = payloads.get(i);
				int sms1 = smsTotal.indexOf("Number: ");
				int sms2 = smsTotal.indexOf("Message: ");
				String smsNumber = smsTotal.substring(sms1 + 8, sms2).trim();
				String smsMessage = smsTotal.substring(sms2 + 9).trim();
				String sms = "sms:".concat(smsNumber).concat("?body=")
						.concat(smsMessage);
				byte[] uriField = sms.getBytes(Charset.forName("US-ASCII"));
				byte[] payload = new byte[uriField.length + 1];
				System.arraycopy(uriField, 0, payload, 1, uriField.length);
				NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
						NdefRecord.RTD_URI, new byte[0], payload);
				ndefRecords.add(nr1);
			} else if (typeAtLoc.startsWith("Geo Location:")) {
				String geoTotal = payloads.get(i);
				int geo1 = geoTotal.indexOf("Latitude: ");
				int geo2 = geoTotal.indexOf("Longitude: ");
				String geoLat = geoTotal.substring(geo1 + 10, geo2).trim();
				String geoLon = geoTotal.substring(geo2 + 11).trim();
				String coords = "geo:".concat(geoLat).concat(",")
						.concat(geoLon);
				byte[] uriField = coords.getBytes(Charset.forName("US-ASCII"));
				byte[] payload = new byte[uriField.length + 1];
				System.arraycopy(uriField, 0, payload, 1, uriField.length);
				NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
						NdefRecord.RTD_URI, new byte[0], payload);
				ndefRecords.add(nr1);
			} else if (typeAtLoc.startsWith("Custom Message:")) {
				String cusTotal = payloads.get(i);
				int cus1 = cusTotal.indexOf("Mime: ");
				int cus2 = cusTotal.indexOf("/");
				int cus3 = cusTotal.indexOf("Message: ");
				String cusMime1 = cusTotal.substring(cus1 + 6, cus2).trim();
				String cusMime2 = cusTotal.substring(cus2 + 1, cus3).trim();
				String cusMess = cusTotal.substring(cus3 + 9).trim();
				String mime = cusMime1.concat("/").concat(cusMime2);
				byte[] uriField = cusMess.getBytes(Charset.forName("US-ASCII"));
				byte[] payload = new byte[uriField.length + 1];
				System.arraycopy(uriField, 0, payload, 1, uriField.length);
				NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
						mime.getBytes(), new byte[0], payload);
				ndefRecords.add(nr1);
			} else if (typeAtLoc.startsWith("Application Record:")) {
				String appRecord = payloads.get(i);
				NdefRecord nr1;
				try {
					nr1 = NdefRecord.createApplicationRecord(appRecord);
				} catch (Exception e) {
					nr1 = NdefRecord
							.createApplicationRecord("com.magicard.trustid");
				}
				ndefRecords.add(nr1);
			} else if (typeAtLoc.startsWith("MAC Address:")) {
				String macAddress = payloads.get(i);
				String bluetoothMimeType = "application/vnd.bluetooth.ep.oob";
				if (macAddress.matches(PATTERN)) {
					String filteredMacAdd = macAddress.replace(":", "");
					filteredMacAdd = filteredMacAdd.replace("-", "");
					filteredMacAdd = Integer.toString(18)
							.concat(filteredMacAdd);
					byte[] uriField = Utility
							.hexStringToByteArray(filteredMacAdd);
					byte[] payload = new byte[uriField.length + 1];
					System.arraycopy(uriField, 0, payload, 1, uriField.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
							bluetoothMimeType.getBytes(), new byte[0], payload);
					ndefRecords.add(nr1);
				} else {
					Toast.makeText(getActivity(), "Invalid MAC Address",
							Toast.LENGTH_SHORT).show();
				}

			} else if (typeAtLoc.startsWith("Contact:")) {
				String contactTotal = payloads.get(i);
				int contact1 = contactTotal.indexOf("Full Name: ");
				int contact2 = contactTotal.indexOf("Company: ");
				int contact3 = contactTotal.indexOf("Phone Number: ");
				int contact4 = contactTotal.indexOf("E-mail Address: ");
				int contact5 = contactTotal.indexOf("Web Page: ");
				String contactFN = contactTotal.substring(contact1 + 11,
						contact2).trim();
				String contactOrg = contactTotal.substring(contact2 + 9,
						contact3).trim();
				String contactPhone = contactTotal.substring(contact3 + 14,
						contact4).trim();
				String contactEmail = contactTotal.substring(contact4 + 16,
						contact5).trim();
				String contactUrl = contactTotal.substring(contact5 + 10)
						.trim();
				String forRecord = "BEGIN:VCARD\nVERSION:3.0\n"
						.concat("FN:" + contactFN + "\n")
						.concat("ORG:" + contactOrg + "\n")
						.concat("TEL:" + contactPhone + "\n")
						.concat("EMAIL:" + contactEmail + "\n")
						.concat("URL:" + contactUrl + "\nEND:VCARD");
				byte[] uriField = forRecord.getBytes(Charset
						.forName("US-ASCII"));
				byte[] payload = new byte[uriField.length + 1];
				System.arraycopy(uriField, 0, payload, 1, uriField.length);
				NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
						"text/vcard".getBytes(), new byte[0], payload);
				ndefRecords.add(nr1);
			} else if (typeAtLoc.startsWith("Social Media:")) {
				String socialTotal = payloads.get(i);
				byte[] uriField = socialTotal.getBytes(Charset
						.forName("US-ASCII"));
				byte[] payload = new byte[uriField.length + 1];
				payload[0] = 0x03;
				System.arraycopy(uriField, 0, payload, 1, uriField.length);
				NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
						NdefRecord.RTD_URI, new byte[0], payload);
				ndefRecords.add(nr1);
			}
		}

	}

}
