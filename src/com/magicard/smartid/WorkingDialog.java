package com.magicard.smartid;

import com.magicard.smartid.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class WorkingDialog extends DialogFragment {
	
	public static final String TAG = "WorkingDialog";
	public static final String EXTRA_MESSAGE = "message";
	
	// Simple dialog creation
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		
		String message = "";
		setRetainInstance(true);
		// If passed a message, will display that, else just "Working..."
		Bundle b = getArguments();
		if(b != null){
			message = b.getString(EXTRA_MESSAGE);
		}else{
			message = "Working...";
		}		
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		View layout = getActivity().getLayoutInflater().inflate(R.layout.activity_working_dialog, null);
		builder.setView(layout);
		
		TextView tvWorking = (TextView) layout.findViewById(R.id.tv_working_message);
		tvWorking.setText(message);
		
		setCancelable(false);
		
		return builder.create();
	}

}
