package com.magicard.smartid;

import java.math.BigInteger;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.widget.Toast;

public class Utility {
	
	public static String byteArrayToHexString(byte[] data) {
		return String.format("%0" + (data.length * 2) + "X", new BigInteger(1,
				data));
	}
	
	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4) + Character
					.digit(s.charAt(i + 1), 16));
		}
		return data;
	}
	
	public static void copyContentToClipboard(String description, String content, Context context) {
		ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Activity.CLIPBOARD_SERVICE);
		ClipData clip = ClipData.newPlainText(
				description, content);
		clipboard.setPrimaryClip(clip);
		Toast.makeText(context,
				"Content added to the clipboard!",
				Toast.LENGTH_SHORT).show();
	}

}
