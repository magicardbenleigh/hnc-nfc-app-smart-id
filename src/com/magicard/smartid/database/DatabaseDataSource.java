package com.magicard.smartid.database;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.view.Gravity;
import android.widget.TableRow;
import android.widget.TextView;

public class DatabaseDataSource {
	
	public static SQLiteDatabase database;
	private DatabaseHelper dbHelper;
	
	private static Cursor cursor = null;
	private Context c;
	
	public static final String ROW_ID = "id";
	public static final String TABLE_ENROLMENT 	= "enrolment";
	public static final String FIELD_FIRST_NAME = "first_name";
	public static final String FIELD_LAST_NAME = "surname";
	public static final String FIELD_COURSE = "course";
	public static final String FIELD_TUTOR = "tutor";
	public static final String FIELD_UID = "uid"; //Unique IDentifier of the student card
	public static final String FIELD_STUDENT_NUMBER = "student_number";
	public static final String FIELD_EXPIRY_DATE = "expiry_date";
	
	public DatabaseDataSource(Context ctx) {
		c = ctx;
		dbHelper = DatabaseHelper.getInstance(c);
	}
	
	// Open method, self-explanatory
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	// Close method, again self-explanatory
	public void close() {
		dbHelper.close();
	}	
	
	// Create the table for storing student data
	public static void createTable() {
		String createTable = "CREATE TABLE IF NOT EXISTS " + TABLE_ENROLMENT + " (" + ROW_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, '" + 
			FIELD_UID + "' TEXT, '" + FIELD_STUDENT_NUMBER + "' TEXT, '" + FIELD_FIRST_NAME + "' TEXT, '" + FIELD_LAST_NAME + "' TEXT, '" + 
			FIELD_COURSE + "' TEXT, '" + FIELD_TUTOR + "' TEXT, '" + FIELD_EXPIRY_DATE + "' TEXT " + ")";
		database.execSQL(createTable);
	}
	
	// Delete table contents - use default table
	public void deleteTable() {
		database.execSQL("DELETE FROM " + TABLE_ENROLMENT);
	}
	
	// Delete table contents - user chooses table
	public static void deleteTable(SQLiteDatabase database) {
		database.execSQL("DELETE FROM " + database);
	}
	
	// Insert a new row into the table
	public String insertRecord(String firstName, String surname, String course, 
			String tutor, String uid, String studentNumber, String expiryDate) {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_FIRST_NAME, firstName);
		cv.put(FIELD_LAST_NAME, surname);
		cv.put(FIELD_COURSE, course);
		cv.put(FIELD_TUTOR, tutor);
		cv.put(FIELD_UID, uid);
		cv.put(FIELD_STUDENT_NUMBER, studentNumber);
		cv.put(FIELD_EXPIRY_DATE, expiryDate);
		long insertResult = database.insert(TABLE_ENROLMENT, null, cv);
		if (insertResult == -1 ) {
			return "Error occurred whilst inserting record";
		} else {
			return "Record inserted!";
		}
	}
	
	// Updates a rows details
	public String updateRecord(String updatePosition, String firstName, String surname, String course, 
			String tutor, String uid, String studentNumber, String expiryDate) {
		ContentValues cv = new ContentValues();
		cv.put(FIELD_FIRST_NAME, firstName);
		cv.put(FIELD_LAST_NAME, surname);
		cv.put(FIELD_COURSE, course);
		cv.put(FIELD_TUTOR, tutor);
		cv.put(FIELD_UID, uid);
		cv.put(FIELD_STUDENT_NUMBER, studentNumber);
		cv.put(FIELD_EXPIRY_DATE, expiryDate);
		int result = database.update(TABLE_ENROLMENT, cv, ROW_ID + " = '" + updatePosition + "'", null);
		if (result == 0) {
			return "Error updating record";
		} else {
			return "Record updated!";
		}
	}
	
	// Remove a row from the table
	public boolean deleteRecord(String rowID) {
		return database.delete("'" + TABLE_ENROLMENT + "'", ROW_ID + " = '" + rowID + "'", null) > 0;
	}
	
	// Get fields, only need first row as otherwise waste of time
	public String[] getFields() {
		cursor = database.query(TABLE_ENROLMENT, null, null, null, null, null, null, "1");
		String[] columnNames = cursor.getColumnNames();
		cursor.close();
		return columnNames;
	}
	
	// Gets number of rows in the table, based on provided query
	public int getRecordAmount(HashMap<String, String> query) {
		String[] arguments = null;
		String where = null;
		
		if(query != null){
			arguments = new String[query.size()];
			where = "";
			int argumentIndex = 0;
			for (Map.Entry<String, String> entry : query.entrySet()) {
			    String key = entry.getKey();
			    String value = entry.getValue();
			    arguments[argumentIndex++] = value;
			    where += "`" + key + "` = ? AND ";
			}
			where = where.substring(0, where.length() - 4);
		}
		
		String[] fields = getFields();
		Arrays.sort(fields);
		for(int i = 0; i < fields.length; i++){
			fields[i] = "`" + fields[i] + "`";
		}
		cursor = database.query(TABLE_ENROLMENT, fields, where, arguments, null, null, null);
		int cursorCount = cursor.getCount();
		cursor.close();
		return cursorCount;
	}
	
	// Get records, create TableRow for each row, then pass result back, no ordering
	public ArrayList<TableRow> getRecords(DatabaseActivity databaseActivity, int offset) {
		return getRecords(databaseActivity, offset, ROW_ID);
	}
	
	// Get records, create TableRow for each row, then pass result back, with ordering
	public ArrayList<TableRow> getRecords(DatabaseActivity dbActivity, int offset, String columnToOrder) {
		cursor = database.rawQuery("SELECT * FROM " + TABLE_ENROLMENT + " ORDER BY " + columnToOrder + " COLLATE NOCASE LIMIT 5 OFFSET "+ offset +";", null);
		ArrayList<TableRow> rowArray = new ArrayList<TableRow>();
		if (cursor != null && cursor.getCount() > 0) {
			for (int i = 0; i < cursor.getCount(); i++) {
				cursor.moveToPosition(i);
				TableRow tr = new TableRow(dbActivity);
				tr.setTag(cursor.getString(cursor.getColumnIndex(ROW_ID)));
				
				String uid = cursor.getString(cursor.getColumnIndex(FIELD_UID));
				TextView tv = createTextView(dbActivity);
				tv.setText(uid);
				tr.addView(tv);
				
				String studentNumber = cursor.getString(cursor.getColumnIndex(FIELD_STUDENT_NUMBER));
				tv = createTextView(dbActivity);
				tv.setText(studentNumber);
				tr.addView(tv);
				
				String firstName = cursor.getString(cursor.getColumnIndex(FIELD_FIRST_NAME));
				tv = createTextView(dbActivity);
				tv.setText(firstName);
				tr.addView(tv);
				
				String surname = cursor.getString(cursor.getColumnIndex(FIELD_LAST_NAME));
				tv = createTextView(dbActivity);
				tv.setText(surname);
				tr.addView(tv);
				
				String course = cursor.getString(cursor.getColumnIndex(FIELD_COURSE));
				tv = createTextView(dbActivity);
				tv.setText(course);
				tr.addView(tv);
				
				String tutor = cursor.getString(cursor.getColumnIndex(FIELD_TUTOR));
				tv = createTextView(dbActivity);
				tv.setText(tutor);
				tr.addView(tv);
				
				String expiryDate = cursor.getString(cursor.getColumnIndex(FIELD_EXPIRY_DATE));
				tv = createTextView(dbActivity);
				tv.setText(expiryDate);
				tr.addView(tv);
				
				rowArray.add(tr);
			}
			cursor.close();
		} else {
			cursor.close();
		}
		return rowArray;
	}
	
	// Get records that match the search query, create TableRow for each row, pass resulting array back
	public ArrayList<TableRow> searchTable(DatabaseActivity dbActivity, HashMap<String, String> query, int offset) {
		
		String[] arguments = null;
		String where = null;
		
		if(query != null) {
			arguments = new String[query.size()];
			where = "";
			int argumentIndex = 0;
			for (Map.Entry<String, String> entry : query.entrySet()) {
			    String key = entry.getKey();
			    String value = entry.getValue();
			    arguments[argumentIndex++] = value;
			    where += "`" + key + "` = ? AND ";
			}
			where = where.substring(0, where.length() - 4);
		}
		
		String[] fields = null;		
		cursor = database.query("`" + TABLE_ENROLMENT + "`", null, null, null, null, null, null, "1");
		fields = cursor.getColumnNames();
		cursor.close();
		Arrays.sort(fields);
		for(int i = 0; i < fields.length; i++) {
			fields[i] = "`" + fields[i] + "`";
		}
		
		cursor = database.query(true, TABLE_ENROLMENT, fields, where, arguments, null, null, null, Integer.toString(offset) + ", 5");
		
		
		ArrayList<TableRow> rowArray = new ArrayList<TableRow>();
		if (cursor != null && cursor.getCount() > 0) {
			for (int i = 0; i < cursor.getCount(); i++) {
				cursor.moveToPosition(i);
				TableRow tr = new TableRow(dbActivity);
				tr.setTag(cursor.getString(cursor.getColumnIndex(ROW_ID)));
				
				String uid = cursor.getString(cursor.getColumnIndex(FIELD_UID));
				TextView tv = createTextView(dbActivity);
				tv.setText(uid);
				tr.addView(tv);
				
				String studentNumber = cursor.getString(cursor.getColumnIndex(FIELD_STUDENT_NUMBER));
				tv = createTextView(dbActivity);
				tv.setText(studentNumber);
				tr.addView(tv);
				
				String firstName = cursor.getString(cursor.getColumnIndex(FIELD_FIRST_NAME));
				tv = createTextView(dbActivity);
				tv.setText(firstName);
				tr.addView(tv);
				
				String surname = cursor.getString(cursor.getColumnIndex(FIELD_LAST_NAME));
				tv = createTextView(dbActivity);
				tv.setText(surname);
				tr.addView(tv);
				
				String course = cursor.getString(cursor.getColumnIndex(FIELD_COURSE));
				tv = createTextView(dbActivity);
				tv.setText(course);
				tr.addView(tv);
				
				String tutor = cursor.getString(cursor.getColumnIndex(FIELD_TUTOR));
				tv = createTextView(dbActivity);
				tv.setText(tutor);
				tr.addView(tv);
				
				String expiryDate = cursor.getString(cursor.getColumnIndex(FIELD_EXPIRY_DATE));
				tv = createTextView(dbActivity);
				tv.setText(expiryDate);
				tr.addView(tv);
				
				rowArray.add(tr);
			}
			cursor.close();
		} else {
			cursor.close();
		}
		return rowArray;
	}
	
	// Create common TextView parameters
	public TextView createTextView(DatabaseActivity databaseActivity) {
		TextView tv = new TextView(databaseActivity);
		TableRow.LayoutParams lp = new TableRow.LayoutParams();
		lp.setMargins(0, 0, 40, 10);
		tv.setLayoutParams(lp);
		tv.setGravity(Gravity.CENTER);
		tv.setTextSize(20);
		tv.setMaxEms(15);
		return tv;
	}

}