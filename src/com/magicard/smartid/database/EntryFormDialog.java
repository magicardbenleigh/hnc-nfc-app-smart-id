package com.magicard.smartid.database;

import java.util.ArrayList;

import com.magicard.smartid.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EntryFormDialog extends DialogFragment {

	EntryFormDialogListener callback;
	public static final String TAG = "EntryFormDialog";
	static Dialog scanTagDialog;

	static boolean getUid;
	static View entryFormLayout;

	AlertDialog entryDialog;

	static ArrayList<String> values;
	static boolean update = false;
	static boolean search = false;
	
	String expiryDate = "";

	// Returns to DatabaseActivity page for each record type
	public interface EntryFormDialogListener {
		public void onAddTableRow(String firstName, String surname, String course, String tutor, String uid,
				String studentNumber, String expiryDate);
		public void onUpdateTableRow(String firstName, String surname, String course, String tutor, String uid,
				String studentNumber, String expiryDate);
		public void onSearchTable(String firstName, String surname, String course, String tutor, String uid,
				String studentNumber, String expiryDate);
		public void undoEnrol(ArrayList<String> formData, boolean update);
	}

	// Set up to ensure WriteToTag is linked to this
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			callback = (EntryFormDialogListener) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement EntryFormDialogListener");
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		entryFormLayout = getActivity().getLayoutInflater().inflate(R.layout.dialog_entry_form, null);
		builder.setView(entryFormLayout);

		// If array list of fields exists, put them into the fields
		Bundle b = getArguments();
		if (b != null) {
			values = b.getStringArrayList("formValues");
			if (values !=null) {
				for (int i = 0; i < values.size(); i++) {
					String currentValue = values.get(i);
					if (i == 0) {
						EditText etFirstName = (EditText) entryFormLayout.findViewById(R.id.et_db_first_name);
						etFirstName.setText(currentValue);
					} else if (i == 1) {
						EditText etSurname = (EditText) entryFormLayout.findViewById(R.id.et_db_surname);
						etSurname.setText(currentValue);
					} else if (i == 2) {
						EditText etCourse = (EditText) entryFormLayout.findViewById(R.id.et_db_course);
						etCourse.setText(currentValue);
					} else if (i == 3) {
						EditText etTutor = (EditText) entryFormLayout.findViewById(R.id.et_db_tutor);
						etTutor.setText(currentValue);
					} else if (i == 4) {
						EditText etUid = (EditText) entryFormLayout.findViewById(R.id.et_db_uid);
						etUid.setText(currentValue);
					} else if (i == 5) {
						EditText etStudentNumber = (EditText) entryFormLayout.findViewById(R.id.et_db_student_number);
						etStudentNumber.setText(currentValue);
					} else if (i == 6) {
						DatePicker dpExpiryDate = (DatePicker) entryFormLayout.findViewById(R.id.db_date_picker);
						String monthDay = currentValue.substring(currentValue.indexOf("-") + 1);
						int year = Integer.parseInt(currentValue.substring(0, currentValue.indexOf("-")));
						int month = Integer.parseInt(monthDay.substring(0, monthDay.indexOf("-"))) - 1;
						int day = Integer.parseInt(monthDay.substring(monthDay.indexOf("-") + 1));
						dpExpiryDate.init(year, month, day, null);
					}
				}
			}
			values = null;
			update = b.getBoolean("updateRecord");
			search = b.getBoolean("searchTable");
		}

		Button btnGetUid = (Button) entryFormLayout.findViewById(R.id.btn_db_get_uid);
		btnGetUid.setOnClickListener(new View.OnClickListener() {

			// Bring up scan tag dialog prompt
			@Override
			public void onClick(View v) {
				getUid = true;
				scanTagDialog = new Dialog(getActivity());
				View layout = getActivity().getLayoutInflater().inflate(R.layout.dialog_scan_tag, null);
				scanTagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
				scanTagDialog.setContentView(layout);
				values = getValues();
				entryDialog.dismiss();
				TextView tv = (TextView) layout.findViewById(R.id.tv_scan_tag_title);
				tv.setText("Scan card to get UID");
				Button cancelButton = (Button) layout.findViewById(R.id.btn_scan_dialog_cancel);
				cancelButton.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						scanTagDialog.dismiss();
						getUid = false;
						search = false;
						update = false;
					}

				});
				scanTagDialog.show();
			}
		});
		
		String buttonText = null;
		if (update) {
			buttonText = "Update";
		} else if (search) {
			buttonText = "Search";
			TextView tv = (TextView) entryFormLayout.findViewById(R.id.tv_database_title);
			tv.setText("Search Form");
			CheckBox cbSearch = (CheckBox) entryFormLayout.findViewById(R.id.cb_db_search);
			cbSearch.setVisibility(View.VISIBLE);
			cbSearch.setChecked(false);
			final DatePicker dpExpiryDate = (DatePicker) entryFormLayout.findViewById(R.id.db_date_picker);
			dpExpiryDate.setEnabled(false);
			cbSearch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					
					if (isChecked) {
						dpExpiryDate.setEnabled(true);
					} else {
						dpExpiryDate.setEnabled(false);
					}
				}
				
			});
		} else {
			buttonText = "Enrol";
		}

		builder.setPositiveButton(buttonText, new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				update = false;
				search = false;
			}
		});

		entryDialog = builder.create();
		entryDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		entryDialog.show();
		entryDialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				EditText etFirstName = (EditText) entryFormLayout.findViewById(R.id.et_db_first_name);
				final String firstName = etFirstName.getText().toString();
				EditText etSurname = (EditText) entryFormLayout.findViewById(R.id.et_db_surname);
				final String surname = etSurname.getText().toString();
				EditText etCourse = (EditText) entryFormLayout.findViewById(R.id.et_db_course);
				final String course = etCourse.getText().toString();
				EditText etTutor = (EditText) entryFormLayout.findViewById(R.id.et_db_tutor);
				final String tutor = etTutor.getText().toString();
				EditText etUid = (EditText) entryFormLayout.findViewById(R.id.et_db_uid);
				final String uid = etUid.getText().toString();
				EditText etStudentNumber = (EditText) entryFormLayout.findViewById(R.id.et_db_student_number);
				final String studentNumber = etStudentNumber.getText().toString();
				DatePicker dpExpiryDate = (DatePicker) entryFormLayout.findViewById(R.id.db_date_picker);
				int day = dpExpiryDate.getDayOfMonth();
				int month = dpExpiryDate.getMonth() + 1; // Month list is zero-based, so need to add 1
				int year = dpExpiryDate.getYear();
				expiryDate = Integer.toString(year) + "-" + Integer.toString(month) + "-" + Integer.toString(day);
				
				if (search) {
					CheckBox cbSearch = (CheckBox) entryFormLayout.findViewById(R.id.cb_db_search);
					if (!cbSearch.isChecked()) {
						expiryDate = "";
					}
					
					if (firstName.length() == 0 && surname.length() == 0 && course.length() == 0 && tutor.length() == 0 && 
							uid.length() == 0 && studentNumber.length() == 0 && expiryDate.length() == 0 ) {
						Toast.makeText(getActivity(), "No parameters entered!", Toast.LENGTH_SHORT).show();	
						entryDialog.dismiss();
						return;
					}
				}
				
				if (!search) {
					if (firstName.length() > 0 && surname.length() > 0 && course.length() > 0 && tutor.length() > 0 
							&& uid.length() > 0 && studentNumber.length() > 0 && expiryDate.length() > 0) {
						entryDialog.dismiss();
						AlertDialog.Builder confirmDialog = new AlertDialog.Builder(getActivity());
						confirmDialog.setTitle("Confirm Entry");
						confirmDialog.setMessage("First Name: " + firstName + "\nSurname: " + surname + "\nCourse: "
								+ course + "\nTutor: " + tutor + "\nUID: " + uid + "\nStudent Number: "
								+ studentNumber + "\nExpiry Date: " + expiryDate);
						confirmDialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
		
							@Override
							public void onClick(DialogInterface dialog, int which) {
								if (update) {
									update = false;
									callback.onUpdateTableRow(firstName, surname, course, tutor, uid, studentNumber, expiryDate);
								} else {
									callback.onAddTableRow(firstName, surname, course, tutor, uid, studentNumber, expiryDate);
								}
							}
						});
						confirmDialog.setNegativeButton("Go Back", new DialogInterface.OnClickListener() {
		
							@Override
							public void onClick(DialogInterface dialog, int which) {
								values = getValues();
								callback.undoEnrol(values, update);
							}
		
						});
						confirmDialog.create().show();
					} else {
						Toast.makeText(getActivity(), "All fields must be populated!", Toast.LENGTH_SHORT).show();
					}
				} else if (search) {
					search = false;
					callback.onSearchTable(firstName, surname, course, tutor, uid, studentNumber, expiryDate);
					entryDialog.dismiss();
				}
			}
		});
		return entryDialog;
	}

	// Gets the value of all the entry fields, and amalgamates them into an arraylist
	public ArrayList<String> getValues() {
		EditText etFirstName = (EditText) entryFormLayout.findViewById(R.id.et_db_first_name);
		String firstName = etFirstName.getText().toString();
		EditText etSurname = (EditText) entryFormLayout.findViewById(R.id.et_db_surname);
		String surname = etSurname.getText().toString();
		EditText etCourse = (EditText) entryFormLayout.findViewById(R.id.et_db_course);
		String course = etCourse.getText().toString();
		EditText etTutor = (EditText) entryFormLayout.findViewById(R.id.et_db_tutor);
		String tutor = etTutor.getText().toString();
		EditText etUid = (EditText) entryFormLayout.findViewById(R.id.et_db_uid);
		String uid = etUid.getText().toString();
		EditText etStudentNumber = (EditText) entryFormLayout.findViewById(R.id.et_db_student_number);
		String studentNumber = etStudentNumber.getText().toString();
		DatePicker dpExpiryDate = (DatePicker) entryFormLayout.findViewById(R.id.db_date_picker);
		int day = dpExpiryDate.getDayOfMonth();
		int month = dpExpiryDate.getMonth() + 1; // Month list is zero-based, so need to add 1
		int year = dpExpiryDate.getYear();
		String expiryDate = Integer.toString(year) + "-" + Integer.toString(month) + "-" + Integer.toString(day);
		ArrayList<String> values = new ArrayList<String>();
		values.add(firstName);
		values.add(surname);
		values.add(course);
		values.add(tutor);
		values.add(uid);
		values.add(studentNumber);
		values.add(expiryDate);
		return values;
	}

}
