package com.magicard.smartid.database;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DatabaseHelper extends SQLiteAssetHelper {
	
	private static DatabaseHelper mInstance = null;

	private static final String DATABASE_NAME = "EnrolmentDatabase.db";
	private static final int DATABASE_VERSION = 6;
	
	public static DatabaseHelper getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new DatabaseHelper(context.getApplicationContext());
		}
		return mInstance;
	}
	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Upgrade --> delete and re-create (for adding a column)
	    if (newVersion > oldVersion) {
	        DatabaseDataSource.deleteTable(db);
	        onCreate(db);
	    } 
	}

}
