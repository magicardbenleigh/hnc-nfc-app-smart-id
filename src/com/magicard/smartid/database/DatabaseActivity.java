package com.magicard.smartid.database;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.magicard.smartid.R;
import com.magicard.smartid.MainActivity;
import com.magicard.smartid.Utility;
import com.magicard.smartid.WorkingDialog;
import com.magicard.smartid.database.EntryFormDialog.EntryFormDialogListener;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class DatabaseActivity extends Activity implements EntryFormDialogListener {
	
	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
	private IntentFilter[] mFilters;
	private String[][] mTechLists;
	
	static Dialog scanTagDialog;
	
	String updatePosition;
	String selectedPosition;
	
	public static TableLayout tableDisplay;
	
	public static DatabaseDataSource designDatabase;
	
	int offset = 0;
	
	HashMap<String, String> querySetUp;
	String columnToOrder = DatabaseDataSource.ROW_ID;
	public static TableRow selectedRow;
	
	boolean breakfast;
	boolean checkID;
	NdefMessage ndefMessage;
	String tagUid;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_database);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
		mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
				getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
		mFilters = new IntentFilter[] { ndef, };
		mTechLists = new String[][] { new String[] { Ndef.class.getName() },
				new String[] { NdefFormatable.class.getName() },
				new String[] { NfcA.class.getName() } };
		
		//New instance, open database, create table (no error if not exists)
		designDatabase = new DatabaseDataSource(this);
		designDatabase.open();
		DatabaseDataSource.createTable();
		setFields(null);
		
		Button btnClearSearch = (Button) findViewById(R.id.btn_clear_search);
		btnClearSearch.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				querySetUp = null;
				setFields(null);
			}
		});
		
		Button btnWriteCard = (Button) findViewById(R.id.btn_db_write_card);
		btnWriteCard.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try {
					ArrayList<String> entryValues = new ArrayList<String>();
					ArrayList<NdefRecord> recordsArray = new ArrayList<NdefRecord>();
					entryValues.add(((String) selectedRow.getTag()).toString());
					entryValues.add(((TextView) selectedRow.getChildAt(2)).getText().toString());
					entryValues.add(((TextView) selectedRow.getChildAt(3)).getText().toString());
					entryValues.add(((TextView) selectedRow.getChildAt(4)).getText().toString());
					entryValues.add(((TextView) selectedRow.getChildAt(5)).getText().toString());
					entryValues.add(((TextView) selectedRow.getChildAt(0)).getText().toString());
					entryValues.add(((TextView) selectedRow.getChildAt(1)).getText().toString());
					entryValues.add(((TextView) selectedRow.getChildAt(6)).getText().toString());
					for (int i = 0; i < entryValues.size(); i++) {
						String data = entryValues.get(i);
						byte[] uriField = data.getBytes(Charset.forName("US-ASCII"));
						byte[] payload = new byte[uriField.length + 1];
						System.arraycopy(uriField, 0, payload, 1, uriField.length);
						recordsArray.add(new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
								NdefRecord.RTD_URI, new byte[0], payload));
					}
					ndefMessage = new NdefMessage(recordsArray.toArray(new NdefRecord[recordsArray.size()]));
					startScanTagDialog("Scan card to write data");
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		Button btnBreakfast = (Button) findViewById(R.id.btn_db_breakfast);
		btnBreakfast.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if (new DateTime().getMinuteOfDay() > 525) {
					Toast.makeText(DatabaseActivity.this, "Cut-off is 8:45!", Toast.LENGTH_SHORT).show();
				} else {
					breakfast = true;
					startScanTagDialog("Scan card to read breakfast data");
				}
			}
		});
		
		Button btnCheckID = (Button) findViewById(R.id.btn_db_check_id);
		btnCheckID.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				checkID = true;
				selectedPosition = "";
				startScanTagDialog("Scan card to check credentials");
			}
		});
	}
	
	// Start the dialog to prompt user to scan a tag
	public void startScanTagDialog(String title) {
		scanTagDialog = new Dialog(DatabaseActivity.this);
		View layout = getLayoutInflater().inflate(R.layout.dialog_scan_tag, null);
		scanTagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		scanTagDialog.setContentView(layout);
		TextView tv = (TextView) layout.findViewById(R.id.tv_scan_tag_title);
		tv.setText(title);
		Button cancelButton = (Button) layout.findViewById(R.id.btn_scan_dialog_cancel);
		cancelButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				breakfast = false;
				checkID = false;
				ndefMessage = null;
				scanTagDialog.dismiss();
			}
		});
		scanTagDialog.show();
	}
	
	// To show options to user
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		getMenuInflater().inflate(R.menu.database_action_bar, menu);
		return true;
	}

	// For pressing back to the main page
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			startActivity(new Intent(DatabaseActivity.this, MainActivity.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			return true;
		case R.id.action_delete_table:
			AlertDialog.Builder dialog = new AlertDialog.Builder(DatabaseActivity.this);
			dialog.setMessage("Are you sure you wish to delete the contents from the table?");
			dialog.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
	
				@Override
				public void onClick(DialogInterface dialog, int which) {
					designDatabase.deleteTable();
					tableDisplay.removeAllViews();
				}
			});
			dialog.setNegativeButton("Cancel", null);
			dialog.create().show();
			return true;
		case R.id.action_add:
			EntryFormDialog entryDialog = new EntryFormDialog();
			entryDialog.show(getFragmentManager(), EntryFormDialog.TAG);
			return true;
		case R.id.action_search_table:
			EntryFormDialog searchDialog = new EntryFormDialog();
			Bundle b = new Bundle();
			b.putBoolean("searchTable", true);
			searchDialog.setArguments(b);
			searchDialog.show(getFragmentManager(), EntryFormDialog.TAG);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// User pressed first record button
	public void onClickFirst(View v) {
		offset = 0;
		setFields(null);
	}
	
	// User pressed previous records button
	public void onClickPrev(View v) {
		offset = offset - 5;
		if (offset < 0) {
			offset = 0;
		}
		setFields(null);
	}
	
	// User pressed next records button
	public void onClickNext(View v) {
		int recordNumber = designDatabase.getRecordAmount(querySetUp);
		offset = offset + 5;
		if (offset > recordNumber - 5) {
			offset = recordNumber - 5;
		} 
		if (offset < 0) {
			offset = 0;
		}
		setFields(null);
	}
	
	// User pressed last records button
	public void onClickLast(View v) {
		int recordNumber = designDatabase.getRecordAmount(querySetUp);
		offset = recordNumber - 5;
		if (offset < 0) {
			offset = 0;
		}
		setFields(null);
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("offset", offset);
		outState.putSerializable("query", querySetUp);
		outState.putString("order", columnToOrder);
		outState.putString("selectedPosition", selectedPosition);
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		offset = savedInstanceState.getInt("offset");
		querySetUp = (HashMap<String, String>) savedInstanceState.getSerializable("query");
		columnToOrder = savedInstanceState.getString("order");
		selectedPosition = savedInstanceState.getString("selectedPosition");
		setFields(null);
	}
	
	// Add field titles
	private void setFields(ArrayList<TableRow> tableRows) {
		String[] columnNames = new String[]{"UID", "Student Number", "First Name", 
											"Surname", "Course", "Tutor", "Expiry Date"};
		tableDisplay = (TableLayout) findViewById(R.id.database_table);
		tableDisplay.removeAllViews();
		TableRow tr = new TableRow(this);
		for (final String field: columnNames) {
			TextView tv = new TextView(this);
			TableRow.LayoutParams lp = new TableRow.LayoutParams();
			lp.setMargins(0, 0, 40, 5);
			tv.setLayoutParams(lp);
			tv.setGravity(Gravity.CENTER);
			tv.setText(field);
			tv.setTextSize(20);
			Typeface boldTypeface = Typeface.defaultFromStyle(Typeface.BOLD);
			tv.setTypeface(boldTypeface);
			tv.setClickable(true);
			tr.addView(tv);
			tv.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					columnToOrder = field.toLowerCase(Locale.getDefault()).replace(" ", "_");
					onClickFirst(null);
					ArrayList<TableRow> tableRows = designDatabase.getRecords(DatabaseActivity.this, offset, 
							columnToOrder);
					setFields(tableRows);
				}
			});
		}
		tableDisplay.addView(tr);
		if (tableRows == null) {
			tableRows = designDatabase.getRecords(this, offset, columnToOrder);
		}
		showTable(tableRows);
	}
	
	// Show the table in the main layout, adding each row individually, perform query if needed
	public void showTable(ArrayList<TableRow> tableRows) {
		TextView tvReadOut = (TextView) findViewById(R.id.tv_record_readout);
		if (querySetUp !=null) {
			tableRows = designDatabase.searchTable(this, querySetUp, offset);
			if (tableRows.size() == 0) {
				tableDisplay.removeAllViews();
				tvReadOut.setText("No records to show");
			}
		}
		for (int i = 0; i < tableRows.size(); i++) {
			final TableRow returnedRow = tableRows.get(i);
			if (i == 0) {
				int recordCount = designDatabase.getRecordAmount(querySetUp);
				String searchActive = "";
				if (querySetUp != null) {
					searchActive = "searched ";
				}
				if ((offset + 1) > (recordCount - 5) && (recordCount - 5) > 0) {
					tvReadOut.setText("Showing " + searchActive + "records " + (recordCount - 4) + " - " + (offset + 5) + " of " + recordCount);
				} else if ((offset + 5) > recordCount) {
					tvReadOut.setText("Showing " + searchActive + "records " + (offset + 1) + " - " + recordCount + " of " + recordCount);
				} else {
					tvReadOut.setText("Showing " + searchActive + "records " + (offset + 1) + " - " + (offset + 5) + " of " + recordCount);
				}
			}
			returnedRow.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					for (int i = 0; i < tableDisplay.getChildCount(); i++) {
						View view = tableDisplay.getChildAt(i);
						view.setBackgroundColor(Color.parseColor("#FEFEFE"));
					}
					returnedRow.setBackgroundColor(Color.parseColor("#55004080"));
					selectedRow = returnedRow;
					selectedPosition = (String) returnedRow.getTag();
				}
			});
			if (((String) returnedRow.getTag()).equals(selectedPosition)) {
				returnedRow.setBackgroundColor(Color.parseColor("#55004080"));
			}
			returnedRow.setOnLongClickListener(new View.OnLongClickListener() {
				
				@Override
				public boolean onLongClick(View v) {
					for (int i = 0; i < tableDisplay.getChildCount(); i++) {
						View view = tableDisplay.getChildAt(i);
						view.setBackgroundColor(Color.parseColor("#FEFEFE"));
					}
					returnedRow.setBackgroundColor(Color.parseColor("#55004080"));
					selectedRow = returnedRow;
					selectedPosition = (String) returnedRow.getTag();
					
					final PopupMenu popup = new PopupMenu(DatabaseActivity.this, returnedRow);
					popup.getMenuInflater().inflate(R.menu.database_row_options,
							popup.getMenu());
					popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {

						@Override
						public boolean onMenuItemClick(MenuItem item) {
							int itemID = item.getItemId();
							
							if (itemID == R.id.action_db_delete) {
								AlertDialog.Builder builder = new AlertDialog.Builder(DatabaseActivity.this);
								builder.setMessage("Are you sure you wish to delete this record? \n\nThis operation CANNOT be reversed!");
								builder.setPositiveButton("Delete",
										new DialogInterface.OnClickListener() {
											@Override
											public void onClick(DialogInterface dialog,
													int which) {
												boolean result = designDatabase.deleteRecord((String) returnedRow.getTag());
												if (result) {
													Toast.makeText(getBaseContext(), "Record deleted!", Toast.LENGTH_SHORT).show();
													onClickFirst(null);
												} else {
													Toast.makeText(getBaseContext(), "Error deleting record", Toast.LENGTH_SHORT).show();
												}
												setFields(null);
											}
										});
											
								builder.setNegativeButton("Cancel", null).show();
							} else if (itemID == R.id.action_db_edit) {
								updatePosition = (String) returnedRow.getTag();
								ArrayList<String> entryValues = new ArrayList<String>();
								entryValues.add(((TextView) returnedRow.getChildAt(2)).getText().toString());
								entryValues.add(((TextView) returnedRow.getChildAt(3)).getText().toString());
								entryValues.add(((TextView) returnedRow.getChildAt(4)).getText().toString());
								entryValues.add(((TextView) returnedRow.getChildAt(5)).getText().toString());
								entryValues.add(((TextView) returnedRow.getChildAt(0)).getText().toString());
								entryValues.add(((TextView) returnedRow.getChildAt(1)).getText().toString());
								entryValues.add(((TextView) returnedRow.getChildAt(6)).getText().toString());
								Bundle b = new Bundle();
								b.putStringArrayList("formValues", entryValues);
								b.putBoolean("updateRecord", true);
								EntryFormDialog entryDialog = new EntryFormDialog();
								entryDialog.setArguments(b);
								entryDialog.show(getFragmentManager(), EntryFormDialog.TAG);
							}
							return false;
						}
						
					});
					popup.show();
					return true;
				}
			});
			tableDisplay.addView(returnedRow);
		}
	}
	
	// Method for when the student wants to claim their free breakfast
	public void readBreakfast(NdefRecord[] records) {
		boolean addRecord = false;
		DateTimeFormatter dtf = DateTimeFormat.forPattern("MMM dd yyyy").withLocale(Locale.US);
		String result = checkCredentials(records);
		if (result.equals("Student confirmed!")) {
			ArrayList<NdefRecord> recordsArray = new ArrayList<NdefRecord>();
			for (NdefRecord record : records) {
				recordsArray.add(record);
			}
			try {
				if (recordsArray.get(8) !=null) {
					String receivedDate = new String(recordsArray.get(8).getPayload(), Charset.forName("US-ASCII")).substring(1);
					DateTime userDate = dtf.parseDateTime(receivedDate).plusDays(1);
					if (new DateTime().isAfter(userDate)) {
						String data = new DateTime().toString(dtf);
						byte[] uriField = data.getBytes(Charset.forName("US-ASCII"));
						byte[] payload = new byte[uriField.length + 1];
						System.arraycopy(uriField, 0, payload, 1, uriField.length);
						recordsArray.set(8, new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
								NdefRecord.RTD_URI, new byte[0], payload));
						addRecord = true;
						Toast.makeText(this, "Valid breakfast request - enjoy!", Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(this, "Breakfast already requested!", Toast.LENGTH_SHORT).show();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				String data = new DateTime().toString(dtf);
				byte[] uriField = data.getBytes(Charset.forName("US-ASCII"));
				byte[] payload = new byte[uriField.length + 1];
				System.arraycopy(uriField, 0, payload, 1, uriField.length);
				if (recordsArray.size() > 8) {
					Toast.makeText(this, "Data Error!", Toast.LENGTH_SHORT).show();
					return;
				} else {
					recordsArray.add(new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
							NdefRecord.RTD_URI, new byte[0], payload));
				}
				addRecord = true;
			}
			if (addRecord) {
				addRecord = false;
				ndefMessage = new NdefMessage(recordsArray.toArray(new NdefRecord[recordsArray.size()]));
				startScanTagDialog("Scan card to write data");
			}
		} else {
			Toast.makeText(this, "Invalid student!", Toast.LENGTH_SHORT).show();
		}
	}
	
	// Method for if the user wants to check if a students card is valid
	public String checkCredentials(NdefRecord[] records) {
		try {
			ArrayList<String> stringArray = new ArrayList<String>();
			for (NdefRecord ndefRecord: records) {
				stringArray.add(new String(ndefRecord.getPayload(), Charset.forName("US-ASCII")).substring(1));
			}
			querySetUp = new HashMap<String, String>();
			querySetUp.put(DatabaseDataSource.FIELD_FIRST_NAME, stringArray.get(1));
			querySetUp.put(DatabaseDataSource.FIELD_LAST_NAME, stringArray.get(2));
			querySetUp.put(DatabaseDataSource.FIELD_COURSE, stringArray.get(3));
			querySetUp.put(DatabaseDataSource.FIELD_TUTOR, stringArray.get(4));
			querySetUp.put(DatabaseDataSource.FIELD_UID, tagUid);
			querySetUp.put(DatabaseDataSource.FIELD_STUDENT_NUMBER, stringArray.get(6));
			querySetUp.put(DatabaseDataSource.FIELD_EXPIRY_DATE, stringArray.get(7));
			offset = 0;
			ArrayList<TableRow> tableArray = designDatabase.searchTable(this, querySetUp, offset);
			setFields(tableArray);
			DateTimeFormatter df = DateTimeFormat.forPattern("yyyy-MM-dd").withLocale(Locale.US);
			DateTime expDate = df.parseDateTime(stringArray.get(7));
			
			if (tableArray.size() == 1) {
				if (new DateTime().isAfter(expDate)) {
					return "Student card expired!";
				} else {
					return "Student confirmed!";
				}
			} else {
				return "Student doesn't exist!";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "Student doesn't exist!";
		}
	}

	// Got this so the app doesn't exit if you scan the tag accidently
	@Override
	public void onResume() {
		super.onResume();
		if (mNfcAdapter != null)
			mNfcAdapter.enableForegroundDispatch(this, mPendingIntent,
					mFilters, mTechLists);
		designDatabase.open();
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mNfcAdapter != null) {
			mNfcAdapter.disableForegroundDispatch(this);
		}
	}
	
	// Get new intent of tag being scanned
	@Override
	public void onNewIntent(Intent intent) {
		handleIntent(intent);
	}
	
	// Get the UID of the tag intent, insert it into form
	public void handleIntent(Intent intent) {
		Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		tagUid = Utility.byteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));
		if (EntryFormDialog.getUid) {
			Bundle b = new Bundle();
			if (EntryFormDialog.update) {
				b.putBoolean("updateRecord", true);
			}
			if (EntryFormDialog.search) {
				b.putBoolean("searchTable", true);
			}
			EntryFormDialog.getUid = false;
			String tagUid = Utility.byteArrayToHexString(intent.getByteArrayExtra(NfcAdapter.EXTRA_ID));
			EntryFormDialog.values.set(4, tagUid);
			ArrayList<String> entryValues = EntryFormDialog.values;
			b.putStringArrayList("formValues", entryValues);
			EntryFormDialog entryDialog = new EntryFormDialog();
			entryDialog.setArguments(b);
			entryDialog.show(getFragmentManager(), EntryFormDialog.TAG);
			EntryFormDialog.scanTagDialog.dismiss();
		} else if (breakfast || checkID) {
			new ReadingTask().execute(tag);
		} else {
			new WritingTask().execute(tag);
		}
	}

	// Return from entry form dialog - inserts a row into the table, updates table with new result
	@Override
	public void onAddTableRow(String firstName, String surname, String course, String tutor, String uid,
			String studentNumber, String expiryDate) {
		String attemptInsert = designDatabase.insertRecord(firstName, surname, course, tutor, uid, studentNumber, expiryDate);
		Toast.makeText(this, attemptInsert, Toast.LENGTH_SHORT).show();
		setFields(null);
		onClickLast(null);
	}

	// Return for if the user didn't enter correct information, restarts the entry form dialog
	@Override
	public void undoEnrol(ArrayList<String> formData, boolean update) {
		Bundle b = new Bundle();
		b.putStringArrayList("formValues", formData);
		b.putBoolean("updateRecord", update);
		EntryFormDialog entryDialog = new EntryFormDialog();
		entryDialog.setArguments(b);
		entryDialog.show(getFragmentManager(), EntryFormDialog.TAG);
	}

	// Return from entry form dialog if user was updating record, then updates table
	@Override
	public void onUpdateTableRow(String firstName, String surname, String course, String tutor, String uid,
			String studentNumber, String expiryDate) {
		String result = designDatabase.updateRecord(updatePosition, firstName, surname, course, tutor, uid, studentNumber, expiryDate);
		Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
		setFields(null);
	}
	
	// Return from performing a query on the database, loads table to show results
	@Override
	public void onSearchTable(String firstName, String surname, String course, String tutor, String uid,
			String studentNumber, String expiryDate) {
		querySetUp = new HashMap<String, String>();
		if (firstName.length() > 0) {
			querySetUp.put(DatabaseDataSource.FIELD_FIRST_NAME, firstName);
		}
		if (surname.length() > 0) {
			querySetUp.put(DatabaseDataSource.FIELD_LAST_NAME, surname);
		}
		if (course.length() > 0) {
			querySetUp.put(DatabaseDataSource.FIELD_COURSE, course);
		}
		if (tutor.length() > 0) {
			querySetUp.put(DatabaseDataSource.FIELD_TUTOR, tutor);
		}
		if (uid.length() > 0) {
			querySetUp.put(DatabaseDataSource.FIELD_UID, uid);
		}
		if (studentNumber.length() > 0) {
			querySetUp.put(DatabaseDataSource.FIELD_STUDENT_NUMBER, studentNumber);
		}
		if (expiryDate.length() > 0) {
			querySetUp.put(DatabaseDataSource.FIELD_EXPIRY_DATE, expiryDate);
		}
		offset = 0;
		ArrayList<TableRow> tableArray = designDatabase.searchTable(this, querySetUp, offset);
		setFields(tableArray);
	}
	
	// Threaded task to write to the tag, threaded so doesn't block the UI from interactions
	private class WritingTask extends AsyncTask<Tag, Void, String> {
		
		WorkingDialog wd = null;
		
		@Override
		protected void onPreExecute() {
			if (scanTagDialog !=null) {
				scanTagDialog.dismiss();
			}
			wd = new WorkingDialog();
			Bundle b = new Bundle();
			b.putString(WorkingDialog.EXTRA_MESSAGE, "Write student card..");
			wd.setArguments(b);
			wd.show(getFragmentManager(), WorkingDialog.TAG);	
		}
		
		@Override
		protected String doInBackground(Tag... params) {
			Tag tag = params[0];
			try {
				int dataSize = ndefMessage.toByteArray().length;
				Ndef ndefInstance = Ndef.get(tag);
				if (ndefInstance !=null) {
					ndefInstance.connect();
					if (ndefInstance.getMaxSize() < dataSize) {
						return "The data is too large for the card!";
					}
					ndefInstance.writeNdefMessage(ndefMessage);
					ndefInstance.close();
					return "The data has been written to the card!";
				} else {
					NdefFormatable ndefFormatableInstance = NdefFormatable.get(tag);
					if (ndefFormatableInstance != null) {
						try {
							ndefFormatableInstance.connect();
							ndefFormatableInstance.format(ndefMessage);
							ndefFormatableInstance.close();
							return "The data has been written to the card!";
						} catch (IOException e) {
							return "The card has not been formatted";
						}
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				return "Writing to card has failed!";
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String string) {
			Toast.makeText(DatabaseActivity.this, string, Toast.LENGTH_SHORT).show();
			wd.dismiss();
		}
		
	}
	
	// Threaded task to read the tag, threaded so doesn't block the UI from interactions
	private class ReadingTask extends AsyncTask<Tag, Void, NdefRecord[]> {
		
		WorkingDialog wd = null;
		
		@Override
		protected void onPreExecute() {
			if (scanTagDialog !=null) {
				scanTagDialog.dismiss();
			}
			wd = new WorkingDialog();
			Bundle b = new Bundle();
			b.putString(WorkingDialog.EXTRA_MESSAGE, "Reading student card..");
			wd.setArguments(b);
			wd.show(getFragmentManager(), WorkingDialog.TAG);	
		}

		@Override
		protected NdefRecord[] doInBackground(Tag... params) {
			try {
				Tag tag = params[0];
				Ndef ndef = Ndef.get(tag);
				ndef.connect();
				NdefMessage ndefMessage = ndef.getNdefMessage();
				if (ndefMessage !=null) {
					NdefRecord[] records = ndefMessage.getRecords();
					ndef.close();
					return records;
				} else {
					ndef.close();
					return null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(NdefRecord[] ndefRecords) {
			if (breakfast) {
				breakfast = false;
				readBreakfast(ndefRecords);
			} else if (checkID) {
				checkID = false;
				String toastMessage = checkCredentials(ndefRecords);
				Toast.makeText(DatabaseActivity.this, toastMessage, Toast.LENGTH_SHORT).show();
			}
			wd.dismiss();
		}

	}

}
