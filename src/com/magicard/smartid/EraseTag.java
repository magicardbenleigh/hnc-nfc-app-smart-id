package com.magicard.smartid;

import java.nio.charset.Charset;
import java.util.ArrayList;

import com.magicard.smartid.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class EraseTag extends Activity {
	
	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
	private IntentFilter[] mFilters;
	private String[][] mTechLists;
	
	NdefRecord[] records;
	ArrayList<NdefRecord> recordsArray;
	boolean chosenRecordsToDelete = false;
	Dialog scanTagDialog;
	
	TableLayout tl;
	TableRow tr;
	TextView tvEraseTitle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_erase_tag);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
		mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
				getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
		mFilters = new IntentFilter[] { ndef, };
		mTechLists = new String[][] { new String[] { Ndef.class.getName() },
				new String[] { NdefFormatable.class.getName() },
				new String[] { NfcA.class.getName() } };
		
		recordsArray = new ArrayList<NdefRecord>();
		tvEraseTitle = (TextView) findViewById(R.id.tv_erase_title);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			startActivity(new Intent(this, MainActivity.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	// Got this so the app doesn't exit if you scan the tag accidently
	@Override
	public void onResume() {
		super.onResume();
		if (mNfcAdapter != null)
			mNfcAdapter.enableForegroundDispatch(this, mPendingIntent,
					mFilters, mTechLists);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mNfcAdapter != null) {
			mNfcAdapter.disableForegroundDispatch(this);
		}
	}
	
	// Save for orientation change, or backgrounding
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
		super.onSaveInstanceState(savedInstanceState);
		savedInstanceState.putParcelableArrayList("recordsArray", recordsArray);
		savedInstanceState.putBoolean("chosenRecordsToDelete", chosenRecordsToDelete);
		savedInstanceState.putString("title", tvEraseTitle.getText().toString());
	}
	
	// Restore from orientation change, or from being backgrounded
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		recordsArray = savedInstanceState.getParcelableArrayList("recordsArray");
		chosenRecordsToDelete = savedInstanceState.getBoolean("chosenRecordsToDelete");
		tvEraseTitle.setText(savedInstanceState.getString("title"));
		tl = (TableLayout) findViewById(R.id.tl_erase_records);
		populateTable(recordsArray);
	}
	
	// When tag is scanned
	@Override
	public void onNewIntent(Intent intent) {
		handleIntent(intent);
	}

	// Look at what has been set by user, and carry out
	// appropriate action
	private void handleIntent(Intent intent) {
		Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		if (chosenRecordsToDelete) {
			if (recordsArray.size() == 0) {
				new EraseAllTask().execute(tag);
			} else {
				new EraseTask().execute(tag);
			}
		} else {
			new ReadingTask().execute(tag);
		}
	}
	
	// Show records when tag scanned
	private void displayNdefRecords(NdefRecord[] ndefRecords) {
		tvEraseTitle.setText("Choose records to delete");
		tl = (TableLayout) findViewById(R.id.tl_erase_records);
		tl.removeAllViews();
		if (ndefRecords != null) {
			for (NdefRecord ndefRecord : ndefRecords) {
				recordsArray.add(ndefRecord);
			}
			populateTable(recordsArray);
		}
	}
	
	// User has pressed the red X on this record
	private void deleteSelectedRecord(int i) {
		recordsArray.remove(i);
		populateTable(recordsArray);
	}
	
	// Dialog to prompt user to scan their tag
	private void startScanTagDialog(String message) {
		scanTagDialog = new Dialog(EraseTag.this);
		View layout = getLayoutInflater().inflate(
				R.layout.dialog_scan_tag, null);
		scanTagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		scanTagDialog.setContentView(layout);
		if (message != null){
			TextView tv = (TextView) layout.findViewById(R.id.tv_scan_tag_title);
			tv.setText(message);
		}
		Button cancelButton = (Button) layout.findViewById(R.id.btn_scan_dialog_cancel);
		cancelButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				scanTagDialog.dismiss();
				chosenRecordsToDelete = false;
				tvEraseTitle.setText("Scan tag to get records");
				recordsArray.clear();
				tl.removeAllViews();
				RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl_btn_cont);
				rl.setVisibility(View.GONE);
			}
			
		});
		scanTagDialog.show();
	}
	
	// Populate table with the records
	private void populateTable(ArrayList<NdefRecord> arrayListRecords) {
		
		RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl_btn_cont);
		rl.setVisibility(View.VISIBLE);
		
		Button btnClear = (Button) findViewById(R.id.btn_erase_clr_all);
		btnClear.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				recordsArray.clear();
				tl.removeAllViews();
				chosenRecordsToDelete = true;
				startScanTagDialog("Scan tag to erase all records");
			}
		});
		
		Button btnDone = (Button) findViewById(R.id.btn_erase_done);
		btnDone.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				chosenRecordsToDelete = true;
				startScanTagDialog("Scan tag to erase records");
			}
		});
		
		tl.removeAllViews();
		if (recordsArray.size() == 1 && new String(recordsArray.get(0).getPayload(), Charset
				.forName("US-ASCII")).equals("")) {
			tvEraseTitle.setText("Tag already empty - scan another!");
		} else {
			for (int i = 0; i < recordsArray.size(); i++) {
				final int recordNumber = i;
				tr = (TableRow) getLayoutInflater().inflate(R.layout.erase_table_row, tl, false);
				tr.setOnLongClickListener(new View.OnLongClickListener() {

					@Override
					public boolean onLongClick(View v) {
						AlertDialog.Builder builder = new AlertDialog.Builder(EraseTag.this);
						final String data = new String(recordsArray.get(recordNumber).getPayload(), Charset
								.forName("US-ASCII"));
						builder.setMessage("Do you wish to copy '" + data + " ' to the clipboard?");
						builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Utility.copyContentToClipboard("content", data, getBaseContext());
									}
								});
						builder.setNegativeButton("No", null).show();
						return false;
					}
				});
				
				ImageView deleteImage = (ImageView) tr.findViewById(R.id.erase_delete_image);
				deleteImage.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						AlertDialog.Builder builder = new AlertDialog.Builder(EraseTag.this);
						builder.setMessage("Are you sure you wish to delete record " + recordNumber + "?\n\nThis operation CANNOT be reversed!");
						builder.setPositiveButton("Delete",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										deleteSelectedRecord(recordNumber);
									}
								});
									
						builder.setNegativeButton("Cancel", null).show();
					}
				});
				
				TextView tv = (TextView) tr.findViewById(R.id.erase_table_item);
				TextView tvNumber = (TextView) tr.findViewById(R.id.erase_record_number);
				tvNumber.setText("Record " + i + ": ");
				NdefRecord record = recordsArray.get(i);
				tv.setText(new String(record.getPayload(), Charset
						.forName("US-ASCII")));
				tl.addView(tr);	
			};
		}
	}
	
	// Threaded reading task
	private class ReadingTask extends AsyncTask<Tag, Void, NdefRecord[]> {
		
		WorkingDialog wd = null;
		
		@Override
		protected void onPreExecute() {
			
			wd = new WorkingDialog();
			Bundle b = new Bundle();
			b.putString(WorkingDialog.EXTRA_MESSAGE, "Reading NDEF records..");
			wd.setArguments(b);
			wd.show(getFragmentManager(), WorkingDialog.TAG);	
		}
	
		@Override
		protected NdefRecord[] doInBackground(Tag... params) {
			try {
				Tag tag = params[0];
				Ndef ndef = Ndef.get(tag);
				ndef.connect();
				NdefMessage ndefMessage = ndef.getNdefMessage();
				if (ndefMessage !=null) {
					records = ndefMessage.getRecords();
					ndef.close();
					return records;
				} else {
					ndef.close();
					return null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}
	
		// Can't update UI thread while doing in background, so using
		// PostExecute to update the listview
		@Override
		protected void onPostExecute(NdefRecord[] ndefRecords) {
			recordsArray.clear();
			displayNdefRecords(records);
			wd.dismiss();
		}
	
	}

	// Writing task, writes blank tag
	private class EraseAllTask extends AsyncTask<Tag, Void, String> {
		
		WorkingDialog wd = null;
		
		@Override
		protected void onPreExecute() {
			
			wd = new WorkingDialog();
			Bundle b = new Bundle();
			b.putString(WorkingDialog.EXTRA_MESSAGE, "Erasing the tag..");
			wd.setArguments(b);
			wd.show(getFragmentManager(), WorkingDialog.TAG);	
		}
		
		@Override
		protected String doInBackground(Tag... params) {
			Tag tag = params[0];
			try {
				Ndef ndefInstance = Ndef.get(tag);
				ndefInstance.connect();
				NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_EMPTY, null, null, null);
				NdefMessage ndefMessage = new NdefMessage(nr1);
				ndefInstance.writeNdefMessage(ndefMessage);
				ndefInstance.close();
				return "Data has been erased!";
				
			} catch (Exception e) {
				e.printStackTrace();
				return "Erasing tag has failed!";
			}
		}
		
		@Override
		protected void onPostExecute(String string) {
			Toast.makeText(EraseTag.this, string, Toast.LENGTH_SHORT).show();
			tl.removeAllViews();
			tvEraseTitle.setText("Scan tag to get records");
			chosenRecordsToDelete = false;
			RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl_btn_cont);
			rl.setVisibility(View.GONE);
			wd.dismiss();
			scanTagDialog.dismiss();
		}
		
	}
	
	// Writing task, writes what is left from user selection
	private class EraseTask extends AsyncTask<Tag, Void, String> {
		
		WorkingDialog wd = null;
		
		@Override
		protected void onPreExecute() {
			
			wd = new WorkingDialog();
			Bundle b = new Bundle();
			b.putString(WorkingDialog.EXTRA_MESSAGE, "Erasing the tag..");
			wd.setArguments(b);
			wd.show(getFragmentManager(), WorkingDialog.TAG);	
		}
		
		@Override
		protected String doInBackground(Tag... params) {
			Tag tag = params[0];
			try {
				Ndef ndefInstance = Ndef.get(tag);
				ndefInstance.connect();
				NdefMessage ndefMessage = new NdefMessage(recordsArray.toArray(new NdefRecord[recordsArray.size()]));
				ndefInstance.writeNdefMessage(ndefMessage);
				ndefInstance.close();
				return "Data has been erased!";
				
			} catch (Exception e) {
				e.printStackTrace();
				return "Erasing tag has failed!";
			}
		}
		
		@Override
		protected void onPostExecute(String string) {
			Toast.makeText(EraseTag.this, string, Toast.LENGTH_SHORT).show();
			tl.removeAllViews();
			tvEraseTitle.setText("Scan tag to get records");
			chosenRecordsToDelete = false;
			RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl_btn_cont);
			rl.setVisibility(View.GONE);
			wd.dismiss();
			scanTagDialog.dismiss();
		}
		
	}

}
