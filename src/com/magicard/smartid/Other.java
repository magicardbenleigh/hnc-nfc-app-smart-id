package com.magicard.smartid;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.security.SecureRandom;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.magicard.smartid.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.nfc.tech.NfcA;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class Other extends Activity {
	
	private NfcAdapter mNfcAdapter;
	private PendingIntent mPendingIntent;
	private IntentFilter[] mFilters;
	private String[][] mTechLists;
	
	boolean makeReadOnly = false;
	boolean passwordProvided = false;
	boolean confirmEncrypt = false;
	boolean readCopy = false;
	boolean writeCopy = false;
	
	NdefMessage ndefMessage;
	
	String userPassword;
	
	Dialog scanTagDialog;
	
	byte[] overallEncryption;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_other);
		getActionBar().setDisplayHomeAsUpEnabled(true);
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
		mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
				getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter ndef = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
		mFilters = new IntentFilter[] { ndef, };
		mTechLists = new String[][] { new String[] { Ndef.class.getName() },
				new String[] { NdefFormatable.class.getName() },
				new String[] { NfcA.class.getName() } };
		
		Button btnReadOnly = (Button) findViewById(R.id.btn_other_read_only);
		btnReadOnly.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				makeReadOnlyDialog();
			}
		});
		
		Button btnEncrypt = (Button) findViewById(R.id.btn_other_encrypt);
		btnEncrypt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				setPassword();
			}
		});
		
		Button btnCopy = (Button) findViewById(R.id.btn_other_copy_tag);
		btnCopy.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				copyTag();
				
			}
		});

	}
	
	// Save for orientation change, or backgrounding
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean("makeReadOnly", makeReadOnly);
	}
	
	// Restore from orientation change, or from being backgrounded
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		makeReadOnly = savedInstanceState.getBoolean("makeReadOnly");
		if (makeReadOnly) {
			scanTagDialog = new Dialog(this);
			View layout = getLayoutInflater().inflate(
					R.layout.dialog_scan_tag, null);
			scanTagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			scanTagDialog.setContentView(layout);
			Button cancelButton = (Button) layout.findViewById(R.id.btn_scan_dialog_cancel);
			cancelButton.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					scanTagDialog.dismiss();
					makeReadOnly = false;	
				}
				
			});
			scanTagDialog.show();
		}
	}
	
	// Page to return to if back pressed
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			startActivity(new Intent(this, MainActivity.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
		}
		return true;
	}
	
	// Got this so the app doesn't exit if you scan the tag accidently
	@Override
	public void onResume() {
		super.onResume();
		if (mNfcAdapter != null)
			mNfcAdapter.enableForegroundDispatch(this, mPendingIntent,
					mFilters, mTechLists);
	}

	@Override
	public void onPause() {
		super.onPause();
		if (mNfcAdapter != null) {
			mNfcAdapter.disableForegroundDispatch(this);
		}
	}
	
	// Amalgamated use of the scanTagDialog
	private void startScanTagDialog(String message) {
		scanTagDialog = new Dialog(Other.this);
		View layout = getLayoutInflater().inflate(
				R.layout.dialog_scan_tag, null);
		scanTagDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		scanTagDialog.setContentView(layout);
		if (message != null){
			TextView tv = (TextView) layout.findViewById(R.id.tv_scan_tag_title);
			tv.setText(message);
		}
		Button cancelButton = (Button) layout.findViewById(R.id.btn_scan_dialog_cancel);
		cancelButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				scanTagDialog.dismiss();
				makeReadOnly = false;
				passwordProvided = false;
				confirmEncrypt = false;
				readCopy = false;
				writeCopy = false;
			}
			
		});
		scanTagDialog.show();
	}
	
	// Create dialog for telling user to scan tag to copy data
	private void copyTag() {
		readCopy = true;
		startScanTagDialog("Scan tag to get data to copy");	
	}
	
	// Create dialog for user to ensure they want to make tag read-only
	private Dialog makeReadOnlyDialog() {
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		dialog.setMessage("Making a tag read-only is an irreversible process: \n\nThis CANNOT be undone, press 'Confirm' to continue");
		dialog.setPositiveButton("Confirm", new OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				makeReadOnly = true;
				startScanTagDialog(null);
			}
		});
		dialog.setNegativeButton("Cancel", null);
		return dialog.show();
	}
	
	// Dialog for user to enter a password for encryption
	private Dialog setPassword() {
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		final View layout = getLayoutInflater().inflate(R.layout.dialog_user_password, null);
		dialog.setView(layout);
		dialog.setPositiveButton("Confirm", new OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				EditText etPassword = (EditText) layout.findViewById(R.id.et_password);
				userPassword = etPassword.getText().toString();
				if (userPassword.matches(".*\\d.*") && 
						userPassword.matches(".*[A-Z].*") && userPassword.matches(".*[a-z].*")) {
					EditText etConfirmPassword = (EditText) layout.findViewById(R.id.et_confirm_password);
					String confirmPassword = etConfirmPassword.getText().toString();
					if (confirmPassword.equals(userPassword)) {
						passwordProvided = true;
						startScanTagDialog("Scan tag to get data to encrypt");
					} else {
						Toast.makeText(Other.this, "Both passwords must match", Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(Other.this, "Password must contain at least one number, "
							+ "and contain both upper and lower case letters", Toast.LENGTH_SHORT).show();
				}
			}
			
		});
		dialog.setNegativeButton("Cancel", null);
		return dialog.show();
		
	}
	
	public static int iterations = 16384;
	public static int lengthOfKey = 128;
	
	// Begin encryption process, this is populating table with records
	private Dialog setEncryptionTable(NdefRecord[] ndefRecords, final NdefMessage ndefMessage) {
		//Just filling up the table so the user can see which record(s) will be encrypted
		AlertDialog.Builder dialog = new AlertDialog.Builder(this);
		View layout = getLayoutInflater().inflate(
				R.layout.dialog_scan_tag, null);
		dialog.setView(layout);
		TextView tvTitle = (TextView) layout.findViewById(R.id.tv_scan_tag_title);
		tvTitle.setText("Scan tag to encrypt this data");
		ImageView iv = (ImageView) layout.findViewById(R.id.iv_scan_image);
		iv.setVisibility(View.GONE);
	
		TableLayout tl = (TableLayout) layout.findViewById(R.id.scan_records_table);
		tl.removeAllViews();
		TableRow tr;
		TextView tv;
		TextView tvNumber;
		int i = 0;
		if (ndefRecords != null) {
			for (final NdefRecord record : ndefRecords) {
				tr = (TableRow) getLayoutInflater().inflate(R.layout.record_table_row, tl, false);
				tr.setOnLongClickListener(new View.OnLongClickListener() {
	
					@Override
					public boolean onLongClick(View v) {
						AlertDialog.Builder builder = new AlertDialog.Builder(
								Other.this);
						final String data = new String(record.getPayload(), Charset
								.forName("US-ASCII"));
						builder.setMessage("Do you wish to copy '" + data + "' to the clipboard?");
						builder.setPositiveButton("Yes",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										Utility.copyContentToClipboard("content", data, getBaseContext());
									}
								});
						builder.setNegativeButton("No", null).show();
						return false;
					}
				});
				
				tv = (TextView) tr.findViewById(R.id.table_item);
				tvNumber = (TextView) tr.findViewById(R.id.record_number);
				tvNumber.setText("Record " + i + ": ");
				tv.setText(new String(record.getPayload(), Charset
						.forName("US-ASCII")));
				tl.addView(tr);	
				i++;
			}
		}
		Button cancelButton = (Button) layout.findViewById(R.id.btn_scan_dialog_cancel);
		cancelButton.setVisibility(View.GONE);
		dialog.setPositiveButton("Encrypt", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				encryptData(ndefMessage);
			}
		});
		dialog.setNegativeButton("Cancel", null);
		return dialog.show();
	}
	
	//Encryption method, includes key stretching
	private void encryptData(final NdefMessage ndefMessage) {
		new Thread() {
			public void run() {
				WorkingDialog wd = new WorkingDialog();
				Bundle b = new Bundle();
				b.putString(WorkingDialog.EXTRA_MESSAGE, "Creating encrypted data..");
				wd.setArguments(b);
				wd.show(getFragmentManager(), WorkingDialog.TAG);	
				confirmEncrypt = true;
				int lengthOfSalt = lengthOfKey / 8;
				
				SecureRandom secRandom = new SecureRandom();
				byte[] salt = new byte[lengthOfSalt];
				secRandom.nextBytes(salt);
				KeySpec keySpec = new PBEKeySpec(userPassword.toCharArray(), salt, iterations, lengthOfKey);
				userPassword = "";
				try {
					SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
					byte[] keyByteArray = keyFactory.generateSecret(keySpec).getEncoded();
					SecretKey secretKey = new SecretKeySpec(keyByteArray, "AES");
					
					Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
					byte[] iv = new byte[cipher.getBlockSize()];
					secRandom.nextBytes(iv);
					IvParameterSpec ivParams = new IvParameterSpec(iv);
					cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivParams);
					byte[] encryptedData = cipher.doFinal(ndefMessage.toByteArray());
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					outputStream.write(encryptedData);
					outputStream.write(iv);
					outputStream.write(salt);
					overallEncryption = outputStream.toByteArray();
					wd.dismiss();
				} catch (Exception e) {
					confirmEncrypt = false;
				    runOnUiThread(new Runnable() {
				        public void run() { 
				            Toast.makeText(Other.this, "Sorry, there was an error during encryption", Toast.LENGTH_SHORT).show();
				        } 
				    });
					e.printStackTrace();
				}
			}
		}.start();
		startScanTagDialog("Scan tag to encrypt data");
	}

	// When tag scanned
	@Override
	public void onNewIntent(Intent intent) {
		handleIntent(intent);
	}

	// Depending on process undertaken by user, perform a task with the tag
	private void handleIntent(Intent intent) {
		Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		if (makeReadOnly) {
			makeReadOnly = false;
			
			Ndef ndef = Ndef.get(tag);
			try {
				ndef.connect();
				boolean canI = ndef.canMakeReadOnly();
				if (canI) {
					boolean hasItHappened = ndef.makeReadOnly();
					if (hasItHappened) {
						Toast.makeText(this, "Tag has been made Read-Only!", Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(this, "Making Read-Only failed!", Toast.LENGTH_SHORT).show();
					}
				} else {
					Toast.makeText(this, "Tag cannot be made Read-Only!", Toast.LENGTH_SHORT).show();
				}
				scanTagDialog.dismiss();
				ndef.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		if (passwordProvided) {
			new ReadingTask().execute(tag);
		}
		
		if (confirmEncrypt) {
			new WritingTask().execute(tag);
		}
		
		if (readCopy) {
			new ReadingTask().execute(tag);
		}
		if (writeCopy) {
			new WritingTask().execute(tag);
		}
	}
	
	// Read the records to be encrypted / read data to be copied
	private class ReadingTask extends AsyncTask<Tag, Void, NdefMessage> {
		
		WorkingDialog wd = null;
		NdefRecord[] records;
		
		@Override
		protected void onPreExecute() {
			
			wd = new WorkingDialog();
			Bundle b = new Bundle();
			b.putString(WorkingDialog.EXTRA_MESSAGE, "Reading NDEF records..");
			wd.setArguments(b);
			wd.show(getFragmentManager(), WorkingDialog.TAG);	
		}

		@Override
		protected NdefMessage doInBackground(Tag... params) {
			try {
				Tag tag = params[0];
				Ndef ndef = Ndef.get(tag);
				ndef.connect();
				NdefMessage encryptionNdefMessage = ndef.getNdefMessage();
				if (encryptionNdefMessage !=null) {
					records = encryptionNdefMessage.getRecords();
					ndef.close();
					return encryptionNdefMessage;
				} else {
					ndef.close();
					return null;
				}
			} catch (Exception e) {
				e.printStackTrace();
				return null;
			}
		}

		@Override
		protected void onPostExecute(NdefMessage encryptionNdefMessage) {
			wd.dismiss();
			scanTagDialog.dismiss();
			if (passwordProvided) {
				passwordProvided = false;
				setEncryptionTable(records, encryptionNdefMessage);
			} else if (readCopy) {
				ndefMessage = encryptionNdefMessage;
				readCopy = false;
				writeCopy = true;
				startScanTagDialog("Scan new tag to write copied data");
			}
		}

	}
	
	// Write the encrypted data / write copied data to new tag
	private class WritingTask extends AsyncTask<Tag, Void, String> {
		
		WorkingDialog wd = null;
		
		@Override
		protected void onPreExecute() {
			scanTagDialog.dismiss();
			wd = new WorkingDialog();
			Bundle b = new Bundle();
			b.putString(WorkingDialog.EXTRA_MESSAGE, "Write tag..");
			wd.setArguments(b);
			wd.show(getFragmentManager(), WorkingDialog.TAG);	
		}
		
		@Override
		protected String doInBackground(Tag... params) {
			Tag tag = params[0];
			try {
				if (confirmEncrypt) {
					byte[] payload = new byte[overallEncryption.length + 1];
					System.arraycopy(overallEncryption, 0, payload, 1, overallEncryption.length);
					NdefRecord nr1 = new NdefRecord(NdefRecord.TNF_MIME_MEDIA,
							"application/com.example.hncnfcapp".getBytes(), new byte[0], payload);
					ndefMessage = new NdefMessage(nr1);
				}
				int dataSize = ndefMessage.toByteArray().length;
				Ndef ndefInstance = Ndef.get(tag);
				if (ndefInstance !=null) {
					ndefInstance.connect();
					if (ndefInstance.getMaxSize() < dataSize) {
						return "The data is too large for the tag!";
					}
					ndefInstance.writeNdefMessage(ndefMessage);
					ndefInstance.close();
					return "The data has been written to the tag!";
				} else {
					NdefFormatable ndefFormatableInstance = NdefFormatable.get(tag);
					if (ndefFormatableInstance != null) {
						try {
							ndefFormatableInstance.connect();
							ndefFormatableInstance.format(ndefMessage);
							ndefFormatableInstance.close();
							return "The data has been written to the tag!";
						} catch (IOException e) {
							return "The tag has not been formatted";
						}
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
				return "Writing to tag has failed! Have you pressed Write?";
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(String string) {
			ndefMessage = null;
			if (confirmEncrypt) {
				confirmEncrypt = false;
				Toast.makeText(Other.this, string, Toast.LENGTH_SHORT).show();
				overallEncryption = "".getBytes();
			} else if (writeCopy) {
				writeCopy = false;
				Toast.makeText(Other.this, string, Toast.LENGTH_SHORT).show();
			}
			wd.dismiss();
		}
		
	}
	
}
